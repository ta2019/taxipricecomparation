package org.bitbucket.taxipricecomparison.dto;

import lombok.Data;

@Data
public class AddressDto {
    private String from;
    private String to;
}
