package org.bitbucket.taxipricecomparison.api.enums;

public enum TaxiClassesEnum {
    econom,
    business,
    comfortplus,
    minivan,
    vip
}
