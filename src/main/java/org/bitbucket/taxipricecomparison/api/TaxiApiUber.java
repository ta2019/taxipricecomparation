package org.bitbucket.taxipricecomparison.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Headers;
import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.json_entity.UberJson;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaxiApiUber extends TaxiApiAbstract {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaxiApiUber.class);

    public TaxiApiUber(RideInfo rideInfo) {
        super(rideInfo);

    }

    @Override
    protected List<RideCard> parseApiResponse(String response) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        UberJson pojo = objectMapper.readValue(response, UberJson.class);
        List<RideCard> rides = new ArrayList<>();
        pojo.getPrices().forEach(ride -> rides.add(ride.formRideCard()));
        return rides;
    }

    @Override
    protected String getBaseUrl() {
        try {
            return ApiUtils.getProperty("uber.url");
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("start_latitude", rideInfo.getFromLat());
        params.put("start_longitude", rideInfo.getFromLong());
        params.put("end_latitude", rideInfo.getToLat());
        params.put("end_longitude", rideInfo.getToLong());
        return params;
    }

    @Override
    protected Headers getHeaders() {
        String token = "";
        try {
            token = "Bearer " + ApiUtils.getProperty("uber.token");
        } catch (IOException e) {
            LOGGER.error("Exception: {}", e.getMessage());
        }
        return new Headers.Builder()
                .add("Authorization", token)
                .add("Accept-Language", "en_US")
                .add("Content-Type", "application/json")
                .build();
    }

}
