package org.bitbucket.taxipricecomparison.api.services;

import org.bitbucket.taxipricecomparison.api.TaxiApi;
import org.bitbucket.taxipricecomparison.api.TaxiApiGett;
import org.bitbucket.taxipricecomparison.api.TaxiApiUber;
import org.bitbucket.taxipricecomparison.api.TaxiApiYandex;
import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;

import java.util.ArrayList;
import java.util.Optional;

public class TaxiServiceFactory implements Runnable{
    public static Optional<TaxiApi> getTaxiService(TaxiServiceEnum taxiService, RideInfo rideInfo) {
        switch (taxiService) {
            case yandex:
                return Optional.of(new TaxiApiYandex(rideInfo));
            case uber:
                return Optional.of(new TaxiApiUber(rideInfo));
            case gett:
                return Optional.of(new TaxiApiGett(rideInfo));
        }
        // TODO what should it return
        return Optional.empty();
    }

    @Override
    public void run() {
    }
}
