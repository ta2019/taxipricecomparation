package org.bitbucket.taxipricecomparison.api.services;

import okhttp3.*;
import org.bitbucket.taxipricecomparison.TaxiPriceComparisonApplication;
import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

public class ApiUtils {
    private ApiUtils() {}

    public static String generateUrlAddress(String baseUrl, Map<String, String> parameters) {
        if (baseUrl == null) {
            return "";
        }
        HttpUrl.Builder urlBuilder = (Objects.requireNonNull(HttpUrl.parse(baseUrl))).newBuilder();
        parameters.forEach(urlBuilder::addQueryParameter);
        return urlBuilder.toString();
    }


    public static String readApiAnswer(String url, Headers headers) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (response.code() >= 400) {
                LOGGER.errorf("Connection error. Url: %s;\n Headers: %s;\n Status code: %s;\n", url, headers.toString(), response.code());
                throw new ConnectException(String.valueOf(response.code()));
            }
            String answerStr = response.body().string();
            LOGGER.debug("API Answer: " + answerStr);
            return answerStr;
        }
    }

    // TODO replace with something another??
    public static String getProperty(String property) throws IOException {
        Properties prop = new Properties();
        prop.load(TaxiPriceComparisonApplication.class.getClassLoader().getResourceAsStream("application.properties"));
        return prop.getProperty(property);
    }

    public static String taxiClassesJoin() {
        StringBuilder sb = new StringBuilder();
        Arrays.stream(TaxiClassesEnum.values()).forEach(taxiClass -> sb.append(taxiClass ).append(","));
        return sb.deleteCharAt(sb.length() - 1).toString();
    }
}
