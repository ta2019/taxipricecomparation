package org.bitbucket.taxipricecomparison.api.services;

import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.entities.Rides;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;

import java.util.Arrays;

public class TaxiServiceApiCaller {
    private RideInfo rideInfo;

    public TaxiServiceApiCaller(RideInfo rideInfo) {
        this.rideInfo = rideInfo;
    }

    public Rides getAllRideCards() {
        Rides rides = new Rides();
        Arrays.stream(TaxiServiceEnum.values())
                .forEach(taxiService -> TaxiServiceFactory.getTaxiService(taxiService, rideInfo)
                        .ifPresent(x -> rides.add(x.requestAllTariffs())));
        return rides;
    }
}
