package org.bitbucket.taxipricecomparison.api.json_entity;

import java.util.ArrayList;
import java.util.List;

public class UberJson {
    private List<UberCard> prices;

    public UberJson() {
        prices = new ArrayList<>();

    }

    public void addUberCard(UberCard uberCard) {
        prices.add(uberCard);
    }

    public List<UberCard> getPrices() {
        return prices;
    }

    public void setPrices(List<UberCard> prices) {
        this.prices = prices;
    }

}
