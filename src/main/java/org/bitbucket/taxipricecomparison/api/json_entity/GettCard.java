package org.bitbucket.taxipricecomparison.api.json_entity;

import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;

public class GettCard {
    private String product_id;
    private String display_name;
    private String currency;
    private String estimate;
    private Double high_estimate;
    private Double low_estimate;

    public RideCard formRideCard() {
        return new RideCard(TaxiServiceEnum.gett, TaxiClassesEnum.valueOf(display_name), high_estimate, estimate);
    }

    public GettCard() {
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public Double getHigh_estimate() {
        return high_estimate;
    }

    public void setHigh_estimate(Double high_estimate) {
        this.high_estimate = high_estimate;
    }

    public Double getLow_estimate() {
        return low_estimate;
    }

    public void setLow_estimate(Double low_estimate) {
        this.low_estimate = low_estimate;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }
}
