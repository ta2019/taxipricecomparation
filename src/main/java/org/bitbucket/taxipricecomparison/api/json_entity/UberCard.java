package org.bitbucket.taxipricecomparison.api.json_entity;

import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;

import java.util.HashMap;
import java.util.Map;

public class UberCard {
    private String localized_display_name;
    private String display_name;
    private Double distance;
    private String product_id;
    private Double high_estimate;
    private Double low_estimate;
    private Double duration;
    private String estimate;
    private String currency_code;
    private static Map<String, TaxiClassesEnum> transformClassToYandex;


    public RideCard formRideCard() {
        return new RideCard(TaxiServiceEnum.uber, transformClassToYandex.get(display_name), high_estimate, estimate);
    }

    public UberCard() {
        transformClassToYandex = new HashMap<String, TaxiClassesEnum>() {{
            put("uberX", TaxiClassesEnum.econom);
            put("uberXL", TaxiClassesEnum.comfortplus);
            put("uberSELECT", TaxiClassesEnum.business);
            put("uberSUV", TaxiClassesEnum.minivan);
            put("uberBlack", TaxiClassesEnum.vip);
        }};
    }

    public String getLocalized_display_name() {
        return localized_display_name;
    }

    public void setLocalized_display_name(String localized_display_name) {
        this.localized_display_name = localized_display_name;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public Double getHigh_estimate() {
        return high_estimate;
    }

    public void setHigh_estimate(Double high_estimate) {
        this.high_estimate = high_estimate;
    }

    public Double getLow_estimate() {
        return low_estimate;
    }

    public void setLow_estimate(Double low_estimate) {
        this.low_estimate = low_estimate;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }
}
