package org.bitbucket.taxipricecomparison.api.json_entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;

@JsonIgnoreProperties
public class YandexCard {
    private String class_name;
    private String class_text;
    private Integer class_level;
    private Double min_price;
    private Double price;
    private String price_text;
    private Double waiting_time;
    private Double time;

    public RideCard formRideCard() {
        return new RideCard(TaxiServiceEnum.yandex, TaxiClassesEnum.valueOf(class_name), price, price_text);
    }

    public YandexCard() {
    }

    public String getClass_name() {
        return class_name;
    }

    public String getClass_text() {
        return class_text;
    }

    public Integer getClass_level() {
        return class_level;
    }

    public Double getMin_price() {
        return min_price;
    }

    public Double getPrice() {
        return price;
    }

    public String getPrice_text() {
        return price_text;
    }

    public Double getWaiting_time() {
        return waiting_time;
    }

    public Double getTime() {
        return time;
    }
}
