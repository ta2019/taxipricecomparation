package org.bitbucket.taxipricecomparison.api.json_entity;


import java.util.ArrayList;
import java.util.List;

public class GettJson {
    private List<GettCard> prices;

    public GettJson() {
        prices = new ArrayList<>();
    }

    public void addUberCard(GettCard gettCard) {
        prices.add(gettCard);
    }

    public List<GettCard> getPrices() {
        return prices;
    }

    public void setPrices(List<GettCard> prices) {
        this.prices = prices;
    }

}
