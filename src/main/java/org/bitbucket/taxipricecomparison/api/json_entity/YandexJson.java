package org.bitbucket.taxipricecomparison.api.json_entity;

import java.util.List;

public class YandexJson {
    private Double distance;
    private List<YandexCard> options;
    private Double time;
    private String time_text;
    private String currency;

    public YandexJson() {
    }

    @Override
    public String toString() {
        return "YandexJson{" +
                "currency='" + currency + '\'' +
                ", distance=" + distance +
//                ", options=" + options.stream().collect(Collectors.joining(", ")) +
                ", time=" + time +
                ", time_text='" + time_text + '\'' +
                '}';
    }

    public String getCurrency() {
        return currency;
    }

    public List<YandexCard> getOptions() {
        return options;
    }

    public Double getDistance() {
        return distance;
    }

    public Double getTime() {
        return time;
    }

    public String getTime_text() {
        return time_text;
    }

}
