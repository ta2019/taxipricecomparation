package org.bitbucket.taxipricecomparison.api;

import org.bitbucket.taxipricecomparison.api.entities.RideCard;

import java.util.List;

public interface TaxiApi {
    List<RideCard> requestAllTariffs();
}
