package org.bitbucket.taxipricecomparison.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Headers;
import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.json_entity.GettJson;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaxiApiGett extends TaxiApiAbstract {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaxiApiGett.class);

    public TaxiApiGett(RideInfo rideInfo) {
        super(rideInfo);
    }

    @Override
    protected List<RideCard> parseApiResponse(String response) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        GettJson pojo = objectMapper.readValue(response, GettJson.class);
        List<RideCard> rides = new ArrayList<>();
        pojo.getPrices().forEach(ride -> rides.add(ride.formRideCard()));
        return rides;
    }

    @Override
    protected String getBaseUrl() {
        try {
            return ApiUtils.getProperty("gett.url");
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("pickup_latitude", rideInfo.getFromLat());
        params.put("pickup_longitude", rideInfo.getFromLong());
        params.put("destination_latitude", rideInfo.getToLat());
        params.put("destination_longitude", rideInfo.getToLong());
        return params;
    }

    @Override
    protected Headers getHeaders() {
        String token = "";
        try {
            token = ApiUtils.getProperty("gett.token");
        } catch (IOException e) {
            LOGGER.error("Exception: {}", e.getMessage());
        }
        return new Headers.Builder()
                .add("Authorization", token)
                .add("Content-Type", "application/json")
                .build();
    }

}
