package org.bitbucket.taxipricecomparison.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Headers;
import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.json_entity.YandexJson;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;


public class TaxiApiYandex extends TaxiApiAbstract {
    public TaxiApiYandex(RideInfo rideInfo) {
        super(rideInfo);
    }

    private String[] getApiKey() {
        try {
            return new String[]{ApiUtils.getProperty("yandex.apikey"), ApiUtils.getProperty("yandex.clid")};
        } catch (IOException e) {
            LOGGER.errorf("Can't find apikey or clid in application.properties. " + e.getMessage());
            return new String[]{"", ""};
        }
    }

    @Override
    List<RideCard> parseApiResponse(String response) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        YandexJson pojo = objectMapper.readValue(response, YandexJson.class);
        List<RideCard> rides = new ArrayList<>();
        pojo.getOptions().forEach(ride -> rides.add(ride.formRideCard()));
        return rides;
    }

    @Override
    String getBaseUrl() {
        try {
            return ApiUtils.getProperty("yandex.url");
        } catch (IOException e) {
            LOGGER.error("Can't find baseUrl in application.properties file. " + e.getMessage());
            return "";
        }
    }

    @Override
    Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        String[] apiKeys = getApiKey();
        params.put("apikey", apiKeys[0]);
        params.put("clid", apiKeys[1]);
        params.put("class", rideInfo.getTaxiClass());
        String rll = convertAddress();
        LOGGER.debug("Yandex RLL: " + rll);
        params.put("rll", rll);
        return params;
    }

    private String convertAddress() {
        return rideInfo.getFromLong() + "," + rideInfo.getFromLat() + "~" + rideInfo.getToLong() + "," + rideInfo.getToLat();
    }

    @Override
    Headers getHeaders() {
        return new Headers.Builder()
                .add("Accept", "application/json")
                .build();
    }
}
