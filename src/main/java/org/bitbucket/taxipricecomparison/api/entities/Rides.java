package org.bitbucket.taxipricecomparison.api.entities;

import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;

import java.util.*;

public class Rides {
    private Map<TaxiClassesEnum, List<RideCard>> rides;

    public Rides() {
        rides = new TreeMap<>();
    }

    public void add(List<RideCard> list) {
        list.forEach(ride -> {
            if (rides.containsKey(ride.getTaxiClass()))
                rides.get(ride.getTaxiClass()).add(ride);
            else {
                ArrayList<RideCard> arr = new ArrayList<>();
                arr.add(ride);
                rides.put(ride.getTaxiClass(), arr);
            }
        });
    }

    public void sortByPrice() {
        rides.forEach((key, value) -> Collections.sort(value));
    }

    public List<RideCard> get(TaxiClassesEnum classesEnum) {
        return rides.get(classesEnum);
    }

    public List<List<RideCard>> getList() {
        return new ArrayList<>(rides.values());

    }

    public Map<TaxiClassesEnum, List<RideCard>> getAll() {
        return rides;
    }
}
