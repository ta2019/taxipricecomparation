package org.bitbucket.taxipricecomparison.api.entities;


import org.bitbucket.taxipricecomparison.api.CoordinateGeoCoder;
import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;
import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.errors.IncorrectAddressException;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

public class RideInfo {
    private String fromLat;
    private String fromLong;
    private String toLat;
    private String toLong;
    private String taxiClass;

    public RideInfo(AddressDto addressDto) throws IncorrectAddressException {
        this(addressDto.getFrom(), addressDto.getTo());
    }

    public RideInfo(String fromLat, String fromLong,
                    String toLat, String toLong,
                    TaxiClassesEnum taxiClass) {
        this(fromLat, fromLong, toLat, toLong);
        this.taxiClass = taxiClass.toString();
    }

    public RideInfo(String fromAddress,
                    String toAddress,
                    TaxiClassesEnum taxiClass) throws IncorrectAddressException {
        this(fromAddress, toAddress);
        this.taxiClass = taxiClass.toString();
    }

    public RideInfo(String fromLat, String fromLong,
                    String toLat, String toLong) {
        this.fromLat = fromLat;
        this.fromLong = fromLong;
        this.toLat = toLat;
        this.toLong = toLong;
        taxiClass = ApiUtils.taxiClassesJoin();
    }

    public RideInfo(String fromAddress,
                    String toAddress) throws IncorrectAddressException {
        String coordinatesFrom = CoordinateGeoCoder.getCoordinateFromAddress(fromAddress);
        String[] coordArr = coordinatesFrom.split("\\s");
        LOGGER.debug("Geocoder: Coordinates from:" + coordArr[0] + " " + coordArr[1]);
        fromLong = coordArr[0];
        fromLat = coordArr[1];
        String coordinatesTo = CoordinateGeoCoder.getCoordinateFromAddress(toAddress);
        coordArr = coordinatesTo.split("\\s");
        LOGGER.debug("Geocoder: Coordinates to:" + coordArr[0] + " " + coordArr[1]);
        toLong = coordArr[0];
        toLat = coordArr[1];
        taxiClass = ApiUtils.taxiClassesJoin();
    }

    public String getFromLat() {
        return fromLat;
    }

    public String getFromLong() {
        return fromLong;
    }

    public String getToLat() {
        return toLat;
    }

    public String getToLong() {
        return toLong;
    }

    public String getTaxiClass() {
        return taxiClass;
    }
}
