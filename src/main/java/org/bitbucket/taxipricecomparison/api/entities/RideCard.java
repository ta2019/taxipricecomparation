package org.bitbucket.taxipricecomparison.api.entities;

import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class RideCard implements Comparable<RideCard> {

    private TaxiServiceEnum taxiService;
    private TaxiClassesEnum taxiClass;
    private double price;
    private String cost;

    public RideCard(TaxiServiceEnum taxiService, TaxiClassesEnum taxiClass, double price, String cost) {
        this.taxiService = taxiService;
        this.taxiClass = taxiClass;
        this.price = price;
        this.cost = cost;
    }

    public double getPrice() {
        return price;
    }

    public String getCost() {
        return cost;
    }

    public TaxiClassesEnum getTaxiClass() {
        return taxiClass;
    }

    public TaxiServiceEnum getTaxiService() {
        return taxiService;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RideCard ride = (RideCard) o;
        return Double.compare(price, ride.price) == 0 &&
                cost.equals(ride.cost) &&
                Objects.equals(taxiClass, ride.taxiClass) &&
                Objects.equals(taxiService, ride.taxiService);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, cost, taxiClass);
    }

    @Override
    public String toString() {
        return "Ride{" +
                "price=" + price +
                ", cost='" + cost + '\'' +
                ", taxiClass='" + taxiClass + '\'' +
                ", taxiService=" + taxiService +
                '}';
    }

    @Override
    public int compareTo(@NotNull RideCard o) {
        if (this == o)
            return 0;
        return price > o.price? 1 : -1;
    }
}
