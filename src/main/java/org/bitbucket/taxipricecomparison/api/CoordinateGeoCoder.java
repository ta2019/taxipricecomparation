package org.bitbucket.taxipricecomparison.api;

import okhttp3.Headers;
import org.bitbucket.taxipricecomparison.TaxiPriceComparisonApplication;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;
import org.bitbucket.taxipricecomparison.errors.IncorrectAddressException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CoordinateGeoCoder {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaxiPriceComparisonApplication.class);
    public static String getCoordinateFromAddress(String address) throws IncorrectAddressException {
        String coordinate = "";
        try {
            JSONArray jo = new JSONObject(ApiUtils.readApiAnswer(ApiUtils.generateUrlAddress(getBaseUrl(), getParams(address)), getHeaders()))
                    .getJSONObject("response")
                    .getJSONObject("GeoObjectCollection")
                    .getJSONArray("featureMember");
            if (jo.length() > 0) {
                coordinate = jo.getJSONObject(0)
                        .getJSONObject("GeoObject")
                        .getJSONObject("Point")
                        .getString("pos");
            } else {
                String log = "Geocoder/getCoordinate: returned an empty answer.";
                LOGGER.error(log);
                throw new IncorrectAddressException(log);
            }
        } catch (IOException e) {
            LOGGER.error("Geocoder/getCoordinate: error while parsing json. " + e.getMessage());
        }
        if (coordinate.equals(""))
            throw new IncorrectAddressException();
        return coordinate;
    }

    private static String getBaseUrl() {
        try {
            return ApiUtils.getProperty("geocoder.url");
        } catch (IOException e) {
            LOGGER.error("url.geocuder is not presented in properties." + e.getMessage());
            return "";
        }
    }

    private static Map<String, String> getParams(String address) {
        Map<String, String> params = new HashMap<>();
        params.put("apikey", getGeoCoderKey());
        params.put("geocode", address);
        params.put("format", "json");
        return params;
    }

    private static String getGeoCoderKey() {
        try {
            return ApiUtils.getProperty("geocoder.apikey");
        } catch (IOException e) {
            LOGGER.error("apikey.geocoder is not presented in properties." + e.getMessage());
            return "";
        }
    }

    private static Headers getHeaders() {
        return new Headers.Builder()
                .build();
    }
}
