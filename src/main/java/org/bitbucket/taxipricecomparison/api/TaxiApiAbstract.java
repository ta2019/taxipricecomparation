package org.bitbucket.taxipricecomparison.api;

import okhttp3.*;
import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

public abstract class TaxiApiAbstract implements TaxiApi {
    RideInfo rideInfo;

    TaxiApiAbstract(RideInfo rideInfo) {
        this.rideInfo = rideInfo;
    }

    @Override
    public List<RideCard> requestAllTariffs() {
        try {
            return parseApiResponse(ApiUtils.readApiAnswer(ApiUtils.generateUrlAddress(getBaseUrl(), getParams()), getHeaders()));
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return Collections.emptyList();
        }
    }

    abstract List<RideCard> parseApiResponse(String response) throws IOException;

    abstract String getBaseUrl();

    abstract Map<String, String> getParams();

    abstract Headers getHeaders();
}


