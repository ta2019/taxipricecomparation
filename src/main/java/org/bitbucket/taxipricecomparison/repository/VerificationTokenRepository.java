package org.bitbucket.taxipricecomparison.repository;

import org.bitbucket.taxipricecomparison.model.User;
import org.bitbucket.taxipricecomparison.model.VerificationToken;
import org.springframework.data.repository.CrudRepository;

public interface VerificationTokenRepository extends CrudRepository<VerificationToken, Long> {
    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);
}
