package org.bitbucket.taxipricecomparison.repository;

import org.bitbucket.taxipricecomparison.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface UserRepository extends CrudRepository<User, Long> {
    Collection<User> findAll();
    User findByEmail(String email);

    void deleteUserByEmail(String email);

}