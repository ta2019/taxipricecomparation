package org.bitbucket.taxipricecomparison.repository;

import org.bitbucket.taxipricecomparison.model.Record;
import org.bitbucket.taxipricecomparison.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RecordRepository extends CrudRepository<Record, Long> {
    List<Record> findByUser(User user);
    void deleteByIdAndUser(Long id, User user);
    void deleteAllByUser(User user);
}