package org.bitbucket.taxipricecomparison;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaxiPriceComparisonApplication {
	public static void main(String[] args) {

	    SpringApplication.run(TaxiPriceComparisonApplication.class, args);
	}

}
