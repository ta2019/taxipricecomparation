package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.model.Record;
import org.bitbucket.taxipricecomparison.model.User;
import org.bitbucket.taxipricecomparison.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class HistoryControllerService implements IHistoryControllerService {
    private RecordService recordService;
    private UserService userService;
    private UserRepository userRepository;

    public HistoryControllerService(RecordService recordService, UserService userService, UserRepository userRepository) {
        this.recordService = recordService;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @Override
    public String showHistoryService(Model model) {
        AddressDto addressDto = new AddressDto();
        model.addAttribute("address", addressDto);
        List<Record> records = recordService.getByUser(userService.currentUser());
        Collections.sort(records);
        model.addAttribute("records", records);
        return "history";
    }

    @Override
    public void removeRow(Long id, String username) {
        User user = userRepository.findByEmail(username);
        List arr = recordService.getByUser(user);
        //if (arr != null && arr.size() > 0) {
            recordService.deleteByIdAndUser(id, user);
        //}
    }

    @Override
    public void removeAllTable(String username) {
        User user = userRepository.findByEmail(username);
        recordService.deleteAllByUser(user);
    }
}
