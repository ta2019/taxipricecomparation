package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.model.Record;
import org.bitbucket.taxipricecomparison.model.User;

import java.util.List;

public interface IRecordService {
    Record add(User user, AddressDto addressDto);
    List<Record> getByUser(User user);
    void deleteByIdAndUser(Long id, User user);
    void deleteAllByUser(User user);
}
