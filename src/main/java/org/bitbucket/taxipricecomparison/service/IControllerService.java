package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

public interface IControllerService {

    public ModelAndView comparePriceService(final AddressDto addressDto, BindingResult result);


    public String callButtonService(String taxiClass, String service);
}
