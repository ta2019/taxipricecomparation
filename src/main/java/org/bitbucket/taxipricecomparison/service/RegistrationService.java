package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.TaxiPriceComparisonApplication;
import org.bitbucket.taxipricecomparison.dto.UserDto;
import org.bitbucket.taxipricecomparison.errors.EmailSendException;
import org.bitbucket.taxipricecomparison.errors.UserAlreadyExistException;
import org.bitbucket.taxipricecomparison.model.User;
import org.bitbucket.taxipricecomparison.model.VerificationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Properties;

@Service
@Transactional
public class RegistrationService implements IRegistrationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaxiPriceComparisonApplication.class);
    final private IUserService userService;

    public RegistrationService(IUserService userService, ApplicationEventPublisher eventPublisher) {
        this.userService = userService;
    }

    @Transactional
    public String registerUserAccountService(UserDto userDto, HttpServletRequest request, BindingResult result, Model model) {
        String viewName;
        if (result.hasErrors()) {
            result.rejectValue("email", "message.reg_error");
            viewName = "registration";
        } else if (!userDto.getPassword().equals(userDto.getConfirmPassword())){
            result.rejectValue("password", "message.passwordError");
            viewName = "registration";
        } else try {
            userService.registerNewUserAccount(userDto, request);
            viewName = "registrationVerify";
        } catch (UserAlreadyExistException ex) {
            LOGGER.error("UserAlreadyExistException while registration : {}", ex.getMessage());
            result.rejectValue("email", "message.regError");
            viewName = "registration";
        } catch (Exception ex) {
            LOGGER.error("Exception while registration : {}\n", ex.getMessage());
//            model.addAttribute("mail_serv_err", "message.emailServiceError");
            result.rejectValue("email", "message.emailServiceError");
            viewName = "registration";
        }
        return viewName;
    }

    public String confirmRegistrationService(Model model, String token) throws IOException {
        String viewName;
        try (InputStream input = TaxiPriceComparisonApplication.class.getClassLoader().getResourceAsStream("messages.properties")) {
            Properties properties = new Properties();
            properties.load(input);
            VerificationToken verificationToken = userService.getVerificationToken(token);
            if (verificationToken == null) {
                String message = properties.getProperty("auth.message.invalidToken");
                model.addAttribute("message", message);
                viewName = "error";
            } else {
                User user = verificationToken.getUser();
                Calendar cal = Calendar.getInstance();
                if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
                    String message = properties.getProperty("auth.message.expired");
                    model.addAttribute("message", message);
                    viewName = "error";
                } else {
                    user.setEnabled(true);
                    userService.saveRegisteredUser(user);
                    viewName = "successRegister";
                }
            }
        }
        return viewName;
    }
}
