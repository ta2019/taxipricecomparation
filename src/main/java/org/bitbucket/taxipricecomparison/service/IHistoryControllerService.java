package org.bitbucket.taxipricecomparison.service;


import org.bitbucket.taxipricecomparison.model.User;
import org.springframework.ui.Model;

public interface IHistoryControllerService {
    String showHistoryService(Model model);
    void removeRow(Long id, String username);
    void removeAllTable(String username);
}
