package org.bitbucket.taxipricecomparison.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class JavaMailSender {
    @Value("${spring.gmail.password}")
    String password = "";
    @Value("${spring.mail.host}")
    String host = "";
    @Value("${spring.mail.port}")
    String port = "";
    @Value("${spring.mail.username}")
    String username= "";
    @Value("${spring.mail.properties.mail.transport.protocol}")
    String mailTransport= "smtp";
    @Value("${spring.mail.properties.mail.smtp.auth}")
    String auth= "";
    @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
    String tls= "";

    @Bean
    public org.springframework.mail.javamail.JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(Integer.parseInt(port));

        mailSender.setUsername(username);

        mailSender.setPassword(password);
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", mailTransport);
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", tls);
        props.put("mail.debug", "true");

        return mailSender;
    }
}
