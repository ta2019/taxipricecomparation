package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.TaxiPriceComparisonApplication;
import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.entities.Rides;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;
import org.bitbucket.taxipricecomparison.api.services.HaversineAlgorithm;
import org.bitbucket.taxipricecomparison.api.services.TaxiServiceApiCaller;
import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.errors.IncorrectAddressException;
import org.bitbucket.taxipricecomparison.errors.TooBigDistanceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Service
@Transactional
public class MainControllerService implements IControllerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaxiPriceComparisonApplication.class);
    private IRecordService recordService;
    private IUserService userService;
    @Value("${max_distance}")
    private double maxDistance = 500.0;
    private RideInfo rideInfo;
    private AddressDto addressDto;

    public MainControllerService(IRecordService recordService, IUserService userService) {
        this.recordService = recordService;
        this.userService = userService;
    }

    public ModelAndView comparePriceService(@ModelAttribute("address") final AddressDto addressDto,
                                            BindingResult result) {
        ModelAndView modelAndView;
        if (result.hasErrors()) {
            LOGGER.info("result has errors");
            modelAndView = new ModelAndView("addressForm");
        } else if (addressDto.getFrom() == null ||
                addressDto.getTo() == null || addressDto.getFrom().equals(addressDto.getTo())) {
            LOGGER.info("User input null address or equals. From: {}; To: {}", addressDto.getFrom() == null ?
                    "null" : addressDto.getFrom(), addressDto.getTo() == null ? "null" : addressDto.getTo());
            result.rejectValue("from", "address.incorrect");
            modelAndView = new ModelAndView("addressForm");
        } else {
            try {
                rideInfo = new RideInfo(addressDto);
                this.addressDto = addressDto;
                double distance = HaversineAlgorithm.HaversineInKM(Double.parseDouble(rideInfo.getFromLat()),
                        Double.parseDouble(rideInfo.getFromLong()), Double.parseDouble(rideInfo.getToLat()),
                        Double.parseDouble(rideInfo.getToLong()));
                if (distance > maxDistance) {
                    throw new TooBigDistanceException(String.valueOf(distance));
                }
                Rides rides = new TaxiServiceApiCaller(rideInfo).getAllRideCards();
                rides.sortByPrice();
                modelAndView = new ModelAndView("result", "allRides", rides.getList());
                if (rides.getList() != null && rides.getList().size() > 0) {
                    recordService.add(userService.currentUser(), addressDto);
                }
            } catch (IncorrectAddressException e) {
                LOGGER.info("User input incorrect address. " + e.getMessage());
                result.rejectValue("from", "address.incorrect");
                modelAndView = new ModelAndView("addressForm");
            } catch (TooBigDistanceException e) {
                LOGGER.info("User input address which distance is too big!" +
                        " Maximum distance is {}, when user wrote {}", maxDistance, e.getMessage());
                result.rejectValue("from", "address.dist_too_big", String.valueOf(maxDistance));
                /*+ " " + maxDistance + "."*/
                modelAndView = new ModelAndView("addressForm");
            }
        }
        return modelAndView;
    }

    public String callButtonService(String taxiClass, String service) {
        String link = "addressForm";
        if (rideInfo != null && addressDto != null) {
            switch (TaxiServiceEnum.valueOf(service)) {
                case yandex:
                    link = String.format("https://3.redirect.appmetrica.yandex.com/route?" +
                                    "start-lat=%s&start-lon=%s&end-lat=%s&end-lon=%s" +
                                    "&level=%s&appmetrica_tracking_id=25395763362139037",
                            rideInfo.getFromLat(), rideInfo.getFromLong(),
                            rideInfo.getToLat(), rideInfo.getToLong(),
                            taxiClass);
                    break;
                case gett:
                    link = "https://gett.com/ru/";
                    break;
                case uber:
                    try {
                        String pickup = URLEncoder.encode(String.format(
                                "{\"addressLine1\":\"%s\",\"provider\":\"google_places\",\"latitude\":%s,\"longitude\":%s}",
                                addressDto.getFrom(), rideInfo.getFromLat(), rideInfo.getFromLong()),
                                StandardCharsets.UTF_8.toString());

                        String drop = URLEncoder.encode(String.format(
                                "{\"addressLine1\":\"%s\",\"provider\":\"google_places\",\"latitude\":%s,\"longitude\":%s}",
                                addressDto.getTo(), rideInfo.getToLat(), rideInfo.getToLong()
                        ), StandardCharsets.UTF_8.toString());
                        link = String.format("https://m.uber.com/looking?drop=%s&pickup=%s", drop, pickup);
                    } catch (UnsupportedEncodingException e) {
                        LOGGER.error("Can't create uber link. {}" + e.getMessage());
                        link = "https://m.uber.com/looking";
                    }
                    break;
            }
        }
        return link;
    }
}
