package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.dto.UserDto;
import org.bitbucket.taxipricecomparison.errors.EmailSendException;
import org.bitbucket.taxipricecomparison.errors.UserAlreadyExistException;
import org.bitbucket.taxipricecomparison.listener.OnRegistrationCompleteEvent;
import org.bitbucket.taxipricecomparison.model.User;
import org.bitbucket.taxipricecomparison.model.VerificationToken;
import org.bitbucket.taxipricecomparison.repository.RoleRepository;
import org.bitbucket.taxipricecomparison.repository.UserRepository;
import org.bitbucket.taxipricecomparison.repository.VerificationTokenRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Service
@Transactional
public class UserService implements IUserService {
    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    private final VerificationTokenRepository tokenRepository;
    final private ApplicationEventPublisher eventPublisher;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, VerificationTokenRepository tokenRepository, ApplicationEventPublisher eventPublisher) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenRepository = tokenRepository;
        this.eventPublisher = eventPublisher;
    }

    @Override
    @Transactional(rollbackFor = EmailSendException.class, propagation = Propagation.REQUIRES_NEW)
    public User registerNewUserAccount(UserDto accountDto, HttpServletRequest request) throws UserAlreadyExistException {
        if (emailExists(accountDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email address: " + accountDto.getEmail());
        }
        final User user = new User();

        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail().toLowerCase());
        user.setEnabled(false);
        user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
        User answer = userRepository.save(user);
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(answer,
                request.getRequestURL().toString().replace("/user/registration", ""), request.getLocale()));
        return answer;
    }

    @Override
    public User getUser(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    public User findUserByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void saveRegisteredUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken newToken = new VerificationToken(token, user);
        tokenRepository.save(newToken);
    }

    @Override
    public User currentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return findUserByEmail(auth.getName());
    }

    private boolean emailExists(String email) {
        User user = userRepository.findByEmail(email);
        return user != null;
    }

}
