package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.model.Record;
import org.bitbucket.taxipricecomparison.model.User;
import org.bitbucket.taxipricecomparison.repository.RecordRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class RecordService implements IRecordService {

    private final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }


    @Override
    public Record add(User user, AddressDto addressDto) {
        Record record = new Record();
        record.setUser(user);
        record.setDate(new Date());
        record.setAddress1(addressDto.getFrom());
        record.setAddress2(addressDto.getTo());
        recordRepository.save(record);
        return record;
    }

    @Override
    public List<Record> getByUser(User user) {
        return recordRepository.findByUser(user);
    }

    @Override
    public void deleteByIdAndUser(Long id, User user) {
        recordRepository.deleteByIdAndUser(id, user);
    }

    @Override
    public void deleteAllByUser(User user) {
        recordRepository.deleteAllByUser(user);
    }
}
