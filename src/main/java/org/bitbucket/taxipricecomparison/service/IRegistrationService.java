package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.dto.UserDto;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface IRegistrationService {
    String confirmRegistrationService(Model model, String token) throws IOException;

    String registerUserAccountService(UserDto userDto, HttpServletRequest request, BindingResult result, Model model);
}
