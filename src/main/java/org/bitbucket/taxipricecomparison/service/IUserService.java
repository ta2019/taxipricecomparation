package org.bitbucket.taxipricecomparison.service;

import org.bitbucket.taxipricecomparison.dto.UserDto;
import org.bitbucket.taxipricecomparison.errors.UserAlreadyExistException;
import org.bitbucket.taxipricecomparison.model.User;
import org.bitbucket.taxipricecomparison.model.VerificationToken;

import javax.servlet.http.HttpServletRequest;

public interface IUserService {

    User findUserByEmail(String email);

    User registerNewUserAccount(UserDto accountDto, HttpServletRequest request) throws UserAlreadyExistException;

    User getUser(String email);

    VerificationToken getVerificationToken(String token);

    void saveRegisteredUser(User user);

    void createVerificationToken(User user, String token);

    User currentUser();
}
