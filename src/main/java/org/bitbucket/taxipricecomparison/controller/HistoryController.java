package org.bitbucket.taxipricecomparison.controller;

import org.bitbucket.taxipricecomparison.service.IHistoryControllerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HistoryController {

    private IHistoryControllerService historyService;

    public HistoryController(IHistoryControllerService historyService) {
        this.historyService = historyService;
    }

    @PostMapping("/removeRow/{id}/{username}")
    public String removeRow(@PathVariable("id") Long id, @PathVariable("username") String username, Model model) {
        model.asMap();
        historyService.removeRow(id, username);
        return "redirect:/" + showHistory(model);
    }

    @PostMapping("/removeAll/{username}")
    public String removeAll(@PathVariable("username") String username, Model model) {
        historyService.removeAllTable(username);
        return "redirect:/" + showHistory(model);
    }

    @GetMapping("/history")
    public String showHistory(Model model){
        String url = historyService.showHistoryService(model);
        return url;
    }
}
