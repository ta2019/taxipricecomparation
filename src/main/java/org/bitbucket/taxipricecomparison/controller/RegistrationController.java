package org.bitbucket.taxipricecomparison.controller;

import org.bitbucket.taxipricecomparison.dto.UserDto;
import org.bitbucket.taxipricecomparison.service.IRegistrationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;

@Controller
public class RegistrationController {
    final private IRegistrationService registrationService;

    public RegistrationController(IRegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping(value = "/user/registration")
    public String showRegForm(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "registration";
    }

    @PostMapping(value = "/user/registration")
    public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid final UserDto userDto,
                                            HttpServletRequest request,
                                            BindingResult result,
                                            Model model) {
        String viewName = registrationService.registerUserAccountService(userDto, request, result, model);
        return new ModelAndView(viewName, "user", userDto);
    }

    @GetMapping("/registrationConfirm")
    public ModelAndView confirmRegistration(Model model, @RequestParam("token") String token) throws IOException {
        String viewName = registrationService.confirmRegistrationService(model, token);
        return new ModelAndView(viewName);
    }
}
