package org.bitbucket.taxipricecomparison.controller;

import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.service.IControllerService;
import org.bitbucket.taxipricecomparison.service.MainControllerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class MainController {
    private IControllerService mainControllerService;

    public MainController(MainControllerService mainControllerService) {
        this.mainControllerService = mainControllerService;
    }

    @RequestMapping("/")
    public RedirectView redirectView(RedirectAttributes attributes) {
        return new RedirectView("/addressForm");
    }

    @GetMapping("/addressForm")
    public String showAddressForm(Model model) {
        AddressDto addressDto = new AddressDto();
        model.addAttribute("address", addressDto);
        return "addressForm";
    }

    @PostMapping("/addressForm")
    public ModelAndView comparePrice(@ModelAttribute("address") final AddressDto addressDto, BindingResult result) {
        return mainControllerService.comparePriceService(addressDto, result);
    }

    @GetMapping("/callService/{class}/{service}")
    public String callButton(@PathVariable("class") String taxiClass, @PathVariable("service") String service) {
        String link = mainControllerService.callButtonService(taxiClass, service);
        return "redirect:" + link;
    }
}
