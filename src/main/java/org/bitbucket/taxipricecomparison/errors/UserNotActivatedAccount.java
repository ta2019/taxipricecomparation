package org.bitbucket.taxipricecomparison.errors;

public class UserNotActivatedAccount extends RuntimeException {

    public UserNotActivatedAccount(String s) {
        super(s);
    }
}
