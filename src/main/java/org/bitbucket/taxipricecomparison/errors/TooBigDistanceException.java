package org.bitbucket.taxipricecomparison.errors;

public class TooBigDistanceException extends Exception{
    public TooBigDistanceException() {
    }

    public TooBigDistanceException(String message) {
        super(message);
    }

    public TooBigDistanceException(String message, Throwable cause) {
        super(message, cause);
    }
}
