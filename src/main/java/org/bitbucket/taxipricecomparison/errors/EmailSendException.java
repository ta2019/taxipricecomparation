package org.bitbucket.taxipricecomparison.errors;

public class EmailSendException extends RuntimeException{
    public EmailSendException(Exception ex) {
    }

    public EmailSendException(String message) {
        super(message);
    }

    public EmailSendException(String message, Throwable cause) {
        super(message, cause);
    }
}
