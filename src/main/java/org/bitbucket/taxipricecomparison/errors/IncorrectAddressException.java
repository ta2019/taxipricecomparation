package org.bitbucket.taxipricecomparison.errors;

public class IncorrectAddressException extends Exception{
    public IncorrectAddressException() {
    }

    public IncorrectAddressException(String message) {
        super(message);
    }

    public IncorrectAddressException(String message, Throwable cause) {
        super(message, cause);
    }
}
