package org.bitbucket.taxipricecomparison.listener;

import org.bitbucket.taxipricecomparison.TaxiPriceComparisonApplication;
import org.bitbucket.taxipricecomparison.errors.EmailSendException;
import org.bitbucket.taxipricecomparison.model.User;
import org.bitbucket.taxipricecomparison.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

@Component
@Controller
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationListener.class);

    private final IUserService service;

    private final JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String username = "";

    public RegistrationListener(IUserService service, JavaMailSender mailSender) {
        this.service = service;
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        service.createVerificationToken(user, token);
        try (InputStream input = TaxiPriceComparisonApplication.class.getClassLoader().getResourceAsStream("messages.properties")) {

            Properties properties = new Properties();
            properties.load(input);
            String recipientAddress = user.getEmail();
            String subject = properties.getProperty("message.email.sub");
            String confirmationUrl = event.getAppUrl() + "/registrationConfirm?token=" + token;

            String message = properties.getProperty("message.regMail");

            SimpleMailMessage email = new SimpleMailMessage();
            email.setFrom(username);
            email.setTo(recipientAddress);
            email.setSubject(subject);
            email.setText(message + confirmationUrl);
            mailSender.send(email);
            LOGGER.debug("Email successfully sent");
        } catch (Exception ex) {
            LOGGER.error("Email exception. {}. Cause {}", ex.getMessage(), ex.getCause());
            Arrays.stream(ex.getStackTrace()).forEach(e -> LOGGER.error(e.toString()));
            throw new EmailSendException(ex);
        }
    }

}