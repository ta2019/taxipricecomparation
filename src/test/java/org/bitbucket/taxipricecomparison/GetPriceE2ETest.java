package org.bitbucket.taxipricecomparison;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.bitbucket.taxipricecomparison.pages.*;
import org.bitbucket.taxipricecomparison.stub.WireMockStub;
import org.hamcrest.CoreMatchers;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class GetPriceE2ETest {
    private WebDriver driver;
    private LoginPage loginPage;
    private RegistrationPage registrationPage;
    private AddressPage addressPage;
    private HistoryPage historyPage;
    private ResultPage resultPage;
    private NavBarForm navBar;

    private String address1 = "Санкт-Петербург, Сытнинская улица, 7";
    private String address2 = "Санкт-Петербург, Улица Блохина, 6";
    private WireMockStub stubServer;
    @LocalServerPort
    private String port;
    private GreenMail greenMail;

    @Value("${spring.profiles.active}")
    private String activeProfile = "";


    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");

        ChromeOptions options = new ChromeOptions();
        if (!activeProfile.equals("test"))
            options.setHeadless(true);

        driver = new ChromeDriver(options);
        String url = "http://localhost:" + port;
        driver.get(url);
        loginPage = new LoginPage(driver, url, 3);
        registrationPage = new RegistrationPage(driver, url, 3);
        addressPage = new AddressPage(driver, url, 3);
        resultPage = new ResultPage(driver, url, 7);
        historyPage = new HistoryPage(driver, url, 7);
        navBar = new NavBarForm(driver, 3);
        // wiremock temp
        String rll = "30.310694,59.957791~30.303822,59.950591";
        stubServer = new WireMockStub(address1, address2, rll);
        stubServer.addGeocoderApiStub();
        stubServer.addYandexTaxiApiStub("def", "def");
        stubServer.addUberTaxiApiStub("def", "def");
        stubServer.addGettTaxiApiStub("def", "def");
        // smtp
        greenMail = new GreenMail(new ServerSetup(2525, "localhost", "smtp"));
        greenMail.start();
        greenMail.setUser("server@localhost", "pass1word");
    }

    @Test
    @Sql("classpath:sql_scripts/create_user.sql")
    public void loginUserAndGetTaxiPriceCompareTest() {
        // login
        assertThat(loginPage.isItLoginPage(), CoreMatchers.equalTo(true));
        loginPage.loginIntoAccount("test@gmail.com", "lol");
        // address form
        assertThat(addressPage.isItAddressPage(), CoreMatchers.equalTo(true));
        addressPage.comparePrices(address1, address2);
        // result form
        assertThat(resultPage.isItResultForm(), CoreMatchers.equalTo(true));
        // navBar : logout
        assertThat(navBar.isItNavBar(), CoreMatchers.equalTo(true));
        navBar.logout();
        assertThat(loginPage.isLogoutSuccessful(), CoreMatchers.equalTo(true));
    }

    @Test
    @Sql("classpath:sql_scripts/create_token.sql")
    public void registerUserAndPriceCompareTest() throws IOException, MessagingException, InterruptedException {
        String login = "Kostychik97@gmail.com",
                password = "test1pass2";
        // login page
        assertThat(loginPage.isItLoginPage(), CoreMatchers.equalTo(true));
        loginPage.goToRegistration();
        // registration page
        assertThat(registrationPage.isItRegistrationPage(), CoreMatchers.equalTo(true));
        registrationPage.registerNewUser(login, password);
        // success register and activation page
        ActivationPage activationPage = new ActivationPage(driver, getActivationLink(), 3);
        Thread.sleep(2000);
        assertThat(activationPage.isItRegistrationVerifyPage(login), CoreMatchers.equalTo(true));
        activationPage.goToActivationPage();
        Thread.sleep(2000);
        assertThat(activationPage.isItSuccessRegisterPage(), CoreMatchers.equalTo(true));
        activationPage.goBackToLogin();
        // login page again
        assertThat(loginPage.isItLoginPage(), CoreMatchers.equalTo(true));
        loginPage.loginIntoAccount(login, password);
        // address form
        assertThat(addressPage.isItAddressPage(), CoreMatchers.equalTo(true));
        addressPage.comparePrices(address1, address2);
        // result form
        assertThat(resultPage.isItResultForm(), CoreMatchers.equalTo(true));
        // navBar : logout
        assertThat(navBar.isItNavBar(), CoreMatchers.equalTo(true));
        navBar.logout();
        assertThat(loginPage.isLogoutSuccessful(), CoreMatchers.equalTo(true));
    }

    private String getActivationLink() throws IOException, MessagingException {
        boolean ok = greenMail.waitForIncomingEmail(1);
        return ok ? greenMail.getReceivedMessages()[0].getContent().toString().split("link:")[1]
                .replace("\r\n", "") : "";
    }

    @Test
    @Sql("classpath:sql_scripts/create_history.sql")
    public void cleanHistoryTest() {
        // login
        assertThat(loginPage.isItLoginPage(), CoreMatchers.equalTo(true));
        loginPage.loginIntoAccount("test2@gmail.com", "lol");
        // address form
        assertThat(addressPage.isItAddressPage(), CoreMatchers.equalTo(true));
        addressPage.comparePrices(address1, address2);
        // navBar : history
        assertThat(navBar.isItNavBar(), CoreMatchers.equalTo(true));
        navBar.history();
        assertThat(historyPage.isItHistoryForm(4), CoreMatchers.equalTo(true));
        historyPage.removeRow(2);
        assertThat(historyPage.isItHistoryForm(3), CoreMatchers.equalTo(true));
        historyPage.removeRow(2);
        assertThat(historyPage.isItHistoryForm(2), CoreMatchers.equalTo(true));
        // clicked compare button -> go to result
        historyPage.makeQuery(1);
        // go back to history
        assertThat(navBar.isItNavBar(), CoreMatchers.equalTo(true));
        navBar.history();
        assertThat(historyPage.isItHistoryForm(3), CoreMatchers.equalTo(true));
        navBar.history();
        historyPage.removeAll();
        assertThat(historyPage.isItHistoryForm(0), CoreMatchers.equalTo(true));

        // navBar : logout
        assertThat(navBar.isItNavBar(), CoreMatchers.equalTo(true));
        navBar.logout();
        assertThat(loginPage.isLogoutSuccessful(), CoreMatchers.equalTo(true));
    }

    @Test
    @Sql("classpath:sql_scripts/create_history.sql")
    public void makeInvalidParametersReq() {
        String longAdr1 = "г. Москва",
                longAdr2 = "край Приморский, г Владивосток";
        makeRequest(longAdr1, longAdr2);
    }

    private void makeRequest(String addr1, String addr2) {

    }

    @After
    @Sql("classpath:sql_scripts/delete_user.sql")
    public void cleanBase() {
        try {
            if (!activeProfile.equals("test"))
                Thread.sleep(3000);
        } catch (InterruptedException ignore) {
        } finally {
            stubServer.closeStub();
            greenMail.stop();
            driver.quit();
        }
    }

}
