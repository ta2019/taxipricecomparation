package org.bitbucket.taxipricecomparison.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddressPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressPage.class);
    private WebDriver driver;
    private String url;
    private String path;
    private WebDriverWait wait;

    private By address1Field = By.id("address1");
    private By address2Field = By.id("address2");
    private By compareButton = By.id("compare_button");

    public AddressPage(WebDriver driver, String url, int wait_time) {
        this.driver = driver;
        this.url = url;
        path = "/addressForm";
        wait = new WebDriverWait(driver, wait_time);
    }

    public boolean isItAddressPage() {
        try {
            if (driver.getCurrentUrl().equals(url + path)) {
                driver.findElement(address1Field);
                driver.findElement(address2Field);
                driver.findElement(compareButton);
                return true;
            }
        } catch (NoSuchElementException e) {
            LOGGER.error(e.toString());
        }
        return false;
    }


    public void comparePrices(String address1, String address2) {
        driver.findElement(address1Field).sendKeys(address1);
        driver.findElement(address2Field).sendKeys(address2);
        driver.findElement(address2Field).submit();
    }
}
