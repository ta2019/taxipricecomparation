package org.bitbucket.taxipricecomparison.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchContextException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HistoryPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(HistoryPage.class);
    private WebDriver driver;
    private String url;
    private String path;
    private WebDriverWait wait;

    private By removeAll = By.id("remove_all_btn");
    private By cleanPage = By.id("clean_page");

    public HistoryPage(WebDriver driver, String url, int wait_time) {
        this.driver = driver;
        this.url = url;
        path = "/history";
        wait = new WebDriverWait(driver, wait_time);
    }

    public boolean isItHistoryForm(int rowNums) {
        try {
            if (driver.getCurrentUrl().equals(url + path) && isItTable(rowNums)) {
                return true;
            }
        } catch (NoSuchElementException | NoSuchContextException e) {
            LOGGER.error("{}. {}", e.getMessage(), e.getStackTrace());
        }
        return false;
    }

    private boolean isItTable(int rowNums) {
        if (rowNums == 0 && driver.findElement(cleanPage).isDisplayed()) {
            return true;
        }
        driver.findElement(removeAll);
        int i = 1;
        try {
            while (driver.findElement(By.id("i_" + i)).isDisplayed() &&
                    driver.findElement(By.id("date_" + i)).isDisplayed() &&
                    driver.findElement(By.id("address1_" + i)).isDisplayed() &&
                    driver.findElement(By.id("address2_" + i)).isDisplayed() &&
                    driver.findElement(By.id("compare_" + i)).isDisplayed() &&
                    driver.findElement(By.id("remove_" + i)).isDisplayed()) {
                i++;
            }
        } catch (NoSuchElementException ignore) {}
        return rowNums == --i;
    }

    public void removeRow(int row) {
        try {
            driver.findElement(By.id("remove_" + row)).click();
        } catch (NoSuchElementException e) {
            LOGGER.error("{}. {}", e.getMessage(), e.getStackTrace());
        }
    }

    public void removeAll() {
        try {
            driver.findElement(removeAll).click();
        } catch (NoSuchElementException e) {
            LOGGER.error("{}. {}", e.getMessage(), e.getStackTrace());
        }
    }

    public void makeQuery(int row) {
        try {
            driver.findElement(By.id("compare_" + row)).click();
        } catch (NoSuchElementException e) {
            LOGGER.error("{}. {}", e.getMessage(), e.getStackTrace());
        }
    }
}
