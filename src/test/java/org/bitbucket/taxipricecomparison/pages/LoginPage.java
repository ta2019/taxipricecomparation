package org.bitbucket.taxipricecomparison.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressPage.class);
    private WebDriver driver;
    private WebDriverWait wait;
    private String url;
    private String path;
    private String logoutPath;

    private By usernameField = By.id("username_field");
    private By passwordField = By.id("password_field");
    private By loginButton = By.id("login_button");
    private By registrationLink= By.id("registration_link");

    public LoginPage(WebDriver driver, String url, int wait_time) {
        this.driver = driver;
        this.url = url;
        this.path = "/login";
        this.logoutPath = "/login?logout=true";
        wait = new WebDriverWait(driver, wait_time);
    }

    public void loginIntoAccount(String login, String password) {
        driver.findElement(usernameField).sendKeys(login);
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(loginButton).click();
    }

    public boolean isItLoginPage() {
        try {
            if (driver.getCurrentUrl().equals(url + path)) {
                driver.findElement(usernameField);
                driver.findElement(passwordField);
                driver.findElement(loginButton);
                driver.findElement(registrationLink);
                return true;
            }
        } catch (NoSuchElementException e) {
            LOGGER.error(e.toString());
        }
        return false;
    }

    public boolean isLogoutSuccessful() {
        return driver.getCurrentUrl().equals(url + logoutPath) &&
                driver.findElement(By.cssSelector(".alert-success > p")).getText().equals("You have been logged out.");
    }

    public void goToRegistration() {
        driver.findElement(registrationLink).click();
    }
}
