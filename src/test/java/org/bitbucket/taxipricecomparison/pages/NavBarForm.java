package org.bitbucket.taxipricecomparison.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NavBarForm {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressPage.class);
    private WebDriver driver;
    private WebDriverWait wait;

    public NavBarForm(WebDriver driver, int wait_time) {
        this.driver = driver;
        wait = new WebDriverWait(driver, wait_time);
    }

    private By mainPage = By.id("home");
    private By history = By.id("history");
    private By logout = By.id("logout");

    public boolean isItNavBar() {
        driver.findElement(mainPage);
        driver.findElement(history);
        driver.findElement(logout);
        return true;
    }

    public void mainPage() {
        driver.findElement(mainPage).click();
    }

    public void history() {
        driver.findElement(history).click();
    }

    public void logout() {
        driver.findElement(logout).click();
    }
}
