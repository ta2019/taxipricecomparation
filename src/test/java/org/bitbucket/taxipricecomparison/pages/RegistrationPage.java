package org.bitbucket.taxipricecomparison.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegistrationPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressPage.class);
    private WebDriver driver;
    private WebDriverWait wait;
    private String url;
    private String path;

    private By usernameField = By.id("email");
    private By passwordField = By.id("password");
    private By confirmPassword = By.id("confirmPassword");
    private By registrationButton = By.id("register_button");
    private By backToLoginLink = By.id("login_link");



    public RegistrationPage(WebDriver driver, String url, int wait_time) {
        this.driver = driver;
        this.url = url;
        this.path = "/user/registration";
        wait = new WebDriverWait(driver, wait_time);
    }

    public void registerNewUser(String email, String password) {
        driver.findElement(usernameField).sendKeys(email);
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(confirmPassword).sendKeys(password);
        driver.findElement(registrationButton).click();
    }

    public boolean isItRegistrationPage() {
        try {
            if (driver.getCurrentUrl().equals(url + path)) {
                driver.findElement(usernameField);
                driver.findElement(passwordField);
                driver.findElement(confirmPassword);
                driver.findElement(registrationButton);
                driver.findElement(backToLoginLink);
                return true;
            }
        } catch (NoSuchElementException e) {
            LOGGER.error(e.toString());
        }
        return false;
    }

}
