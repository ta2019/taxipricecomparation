package org.bitbucket.taxipricecomparison.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActivationPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddressPage.class);
    private WebDriver driver;
    private WebDriverWait wait;
    private String url;

    private By backToLoginLink = By.id("login_link");
    private By successMessage = By.id("success_message");
    private By successDescription = By.id("success_desc");

    public ActivationPage(WebDriver driver, String url, int wait_time) {
        this.driver = driver;
        this.url = url;
        wait = new WebDriverWait(driver, wait_time);
    }

    public void goToActivationPage() {
        driver.get(url);
    }

    public void goBackToLogin() {
        driver.findElement(backToLoginLink).click();
    }

    public boolean isItSuccessRegisterPage() {
        try {
            if (driver.getCurrentUrl().equals(url)) {
                driver.findElement(backToLoginLink);
                WebElement successElement = driver.findElement(successMessage),
                        successDescElement = driver.findElement(successDescription);
                //TODO: map to properties
                String successRegisterMessage = "You registered successfully.",
                        successMessageText = "Your account successfully confirmed. Now you can login!";
                if (successElement.getText().toLowerCase().equals(successRegisterMessage.toLowerCase())) {
                    if (successDescElement.getText().toLowerCase().equals(successMessageText.toLowerCase())) {
                        return true;
                    } else {
                        LOGGER.error("Elements aren't equal: {} vs {}",
                                successDescElement.getText().toLowerCase(), successMessageText.toLowerCase());
                    }
                } else {
                    LOGGER.error("Elements aren't equal: {} vs {}",
                            successElement.getText().toLowerCase(), successRegisterMessage.toLowerCase());
                }
            }
        } catch (NoSuchElementException e) {
            LOGGER.error("No such element: {}; {}", e.getMessage(), e.getAdditionalInformation());
        }
        return false;
    }

    public boolean isItRegistrationVerifyPage(String email) {
        try {
            driver.findElement(backToLoginLink);
            WebElement successElement = driver.findElement(successMessage),
                    successDescElement = driver.findElement(successDescription);
            //TODO: map to properties
            String successMessageText = "You registered successfully.",
                    successDescriptionText = "We will send you a confirmation message to " + email + ".";
            if (successElement.getText().toLowerCase().equals(successMessageText.toLowerCase())) {
                if (successDescElement.getText().toLowerCase().equals(successDescriptionText.toLowerCase())) {
                    return true;
                } else {
                    LOGGER.error("Elements aren't equal: {} vs {}",
                            successDescElement.getText().toLowerCase(), successDescriptionText.toLowerCase());
                }
            } else {
                LOGGER.error("Elements aren't equal: {} vs {}",
                        successElement.getText().toLowerCase(), successMessageText.toLowerCase());
            }
        } catch (NoSuchElementException e) {
            LOGGER.error("No such element: {}; {}", e.getMessage(), e.getAdditionalInformation());
        }
        return false;
    }
}
