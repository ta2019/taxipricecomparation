package org.bitbucket.taxipricecomparison.pages;

import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class ResultPage {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultPage.class);
    private WebDriver driver;
    private String url;
    private String path;
    private WebDriverWait wait;

    public ResultPage(WebDriver driver, String url, int wait_time) {
        this.driver = driver;
        this.url = url;
        path = "/addressForm";
        wait = new WebDriverWait(driver, wait_time);
    }

    public boolean isItResultForm() {
        try {
            if (driver.getCurrentUrl().equals(url + path)) {
                Arrays.stream(TaxiClassesEnum.values()).forEach(en -> driver.findElement(getClassNameFromNav(en.toString())));
                // find econom link at page and look for his parent
                hasClass(driver.findElement(getClassNameFromNav(TaxiClassesEnum.econom.toString()))
                        .findElement(By.xpath("//ancestor::li")), "active");
                if (!driver.findElement(By.id("econom")).getAttribute("class").contains("active in"))
                    throw new NoSuchContextException("The first card hasn't opened! The econom link don't have class active in.");
                for (TaxiClassesEnum taxiClass : TaxiClassesEnum.values()) {
                    driver.findElement(getClassNameFromNav(taxiClass.toString())).click();
                    for (TaxiServiceEnum taxiService : TaxiServiceEnum.values()) {
                        WebElement element = driver.findElement(By.id(taxiClass.toString() + '_' + taxiService.toString()));
                        wait.until(ExpectedConditions.visibilityOf(element));
                        elementContainsTextIgnoreCase(element, taxiService.toString());
                        element = driver.findElement(By.id(taxiClass.toString() + "_price_" + taxiService.toString()));
                        wait.until(ExpectedConditions.visibilityOf(element));
                        elementContainsTextIgnoreCase(element, "руб.");
                        element = driver.findElement(By.id(taxiClass.toString() + '_' + taxiService.toString() + "_call_btn"));
                        wait.until(ExpectedConditions.visibilityOf(element));
                        elementContainsTextIgnoreCase(element, "Call");
                    }
                }
                return true;
            }
        } catch (NoSuchElementException | NoSuchContextException e) {
            LOGGER.error("{}. {}", e.getMessage(), e.getStackTrace());
        }
        return false;
    }

    private void elementContainsTextIgnoreCase(WebElement element, String expText) {
        if (!element.getText().toLowerCase().contains(expText.toLowerCase())) {
            throw new NoSuchContextException("This element: " + element.toString() +
                    " doesn't contain this text: " + expText +
                    " ,but it contains: " + element.getText());
        }
    }

    private By getClassNameFromNav(String className) {
        return By.xpath(String.format("//*[@href=\"#%s\"]", className));
    }

    private void hasClass(WebElement element, String searchClass) {
        String classes = element.getAttribute("class");
        for (String c : classes.split(" ")) {
            if (c.equals(searchClass)) {
                return;
            }
        }
        throw new NoSuchElementException("Can't find " + searchClass +
                " in element " + element.getTagName() +
                " with text " + element.getText());
    }

}
