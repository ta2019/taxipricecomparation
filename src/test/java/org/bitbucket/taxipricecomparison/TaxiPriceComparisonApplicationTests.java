package org.bitbucket.taxipricecomparison;

import org.bitbucket.taxipricecomparison.controller.RegistrationController;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;

import org.hamcrest.CoreMatchers;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaxiPriceComparisonApplicationTests {
    private String url = "http://localhost:";
    @LocalServerPort
    private int port;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private RegistrationController controller;
    // check that server started and url is valid, so it return answer
    @Test
    public void contextLoads() {
        assertThat(webApplicationContext, CoreMatchers.notNullValue());
        assertThat(controller, CoreMatchers.notNullValue());
    }
}
