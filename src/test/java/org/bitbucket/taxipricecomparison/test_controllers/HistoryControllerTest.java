package org.bitbucket.taxipricecomparison.test_controllers;

import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.service.RecordService;
import org.bitbucket.taxipricecomparison.service.UserService;
import org.bitbucket.taxipricecomparison.stub.WireMockStub;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HistoryControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private RecordService recordService;
    @Autowired
    private UserService userService;
    private MockMvc mockMvc;
    private WireMockStub stubServer;
    private String address1 = "Санкт-Петербург, Сытнинская улица, 7";
    private String address2 = "Санкт-Петербург, Улица Блохина, 6";
    private String historyFormView;
    private String removeRow;
    private String removeAll;

    @Before
    public void setup() {
        historyFormView = "history";
        removeAll = "removeAll";
        removeRow = "removeRow";
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        String rll = "30.310694,59.957791~30.303822,59.950591";
        stubServer = new WireMockStub(address1, address2, rll);
    }

    @After
    public void shutdown() {
        stubServer.closeStub();
    }

    @Test
    public void smokeTest() {
        assertThat(context, CoreMatchers.notNullValue());
        assertThat(mockMvc, CoreMatchers.notNullValue());
    }

    private void addHistory() {
        AddressDto addressDto = new AddressDto();
        addressDto.setFrom(address1);
        addressDto.setTo(address2);
        recordService.add(userService.currentUser(), addressDto);
        addressDto.setFrom(address2);
        addressDto.setTo(address1);
        recordService.add(userService.currentUser(), addressDto);
    }

    @Test
    @WithMockUser
    public void getHistoryForm() throws Exception {
        mockMvc.perform(get("/" + historyFormView)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(historyFormView))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("address", CoreMatchers.equalTo(new AddressDto())))
                .andExpect(model().attribute("records", CoreMatchers.notNullValue()));
    }

    @Test
    @WithMockUser(username = "test@gmail.com")
    public void removeRow() throws Exception {
        addHistory();
        mockMvc.perform(post("/" + removeRow + "/1/test@gmail.com"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/" + historyFormView))
                .andExpect(model().hasNoErrors());
    }

    @Test
    @WithMockUser(username = "test@gmail.com")
    public void removeAll() throws Exception {
        addHistory();
        mockMvc.perform(post("/" + removeAll + "/test@gmail.com"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/" + historyFormView))
                .andExpect(model().hasNoErrors());
    }

    @Test
    @WithMockUser
    public void removeRowEmpty() throws Exception {
        mockMvc.perform(post("/" + removeRow + "/1/test@gmail.com"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/" + historyFormView))
                .andExpect(model().hasNoErrors());
        mockMvc.perform(post("/" + removeAll + "/test@gmail.com"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/" + historyFormView))
                .andExpect(model().hasNoErrors());
    }
}
