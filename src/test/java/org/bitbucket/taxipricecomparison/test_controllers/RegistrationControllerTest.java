package org.bitbucket.taxipricecomparison.test_controllers;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.bitbucket.taxipricecomparison.dto.UserDto;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RegistrationControllerTest {
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    private GreenMail greenMail;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        // smtp
        greenMail = new GreenMail(new ServerSetup(2525, "localhost", "smtp"));
        greenMail.start();
        greenMail.setUser("server@localhost", "pass1word");
    }

    @After
    public void shutdown() {
        greenMail.stop();
    }


    private ResultActions registerUser(String mail, String pass) throws Exception {

        return mockMvc.perform(post("/user/registration")
                .with(csrf())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("email", mail)
                .param("password", pass)
                .param("confirmPassword", pass)
        );
    }

    @Test
    @WithAnonymousUser
    public void registerUserTest() throws Exception {
        String mail = "itmo@gmail.com",
                pass = "password";
        UserDto expectedDto = new UserDto();
        expectedDto.setEmail(mail);
        expectedDto.setPassword(pass);
        expectedDto.setConfirmPassword(pass);
        registerUser(mail, pass)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", CoreMatchers.equalTo(expectedDto)))
                .andExpect(view().name("registrationVerify"))
                .andExpect(content().string(containsString("We will send you a confirmation message to " + mail)));
    }

    @Test
    @WithAnonymousUser
    public void doubleRegisterUserTest() throws Exception {
        String mail = "itmo@gmail.com",
                pass = "password";
        registerUser(mail, pass)
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasErrors("user"));
    }

    @Test
    @WithAnonymousUser
    public void submitRegistrationWithInvalidEmailTest() throws Exception {
        mockMvc.perform(
                post("/user/registration")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("email", "itmo@123")
                        .param("password", "password")
                        .param("confirmPassword", "password")
        )
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    public void submitRegistrationPasswordNotMatchesTest() throws Exception {
        mockMvc.perform(
                post("/user/registration")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("email", "itmo@gmail.com")
                        .param("password", "password")
                        .param("confirmPassword", "drowssap")
        )
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithAnonymousUser
    public void userShouldNotLogin() throws Exception {
        mockMvc.perform(formLogin().user("itmo@gmail.com").password("password"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login?error=true"));
    }

    @Test
    @WithAnonymousUser
    public void userAfterRegisterShouldLogin() throws Exception {
        String mail = "new@gmail.com",
                pass = "password";
        registerUser(mail, pass);
        boolean ok = greenMail.waitForIncomingEmail(1);
        String link = ok ? greenMail.getReceivedMessages()[0].getContent().toString().split("link:")[1]
                .replace("\r\n", "") : "";
        mockMvc.perform(get(link));
        mockMvc.perform(formLogin().user(mail).password(pass))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/addressForm"));
    }
}
