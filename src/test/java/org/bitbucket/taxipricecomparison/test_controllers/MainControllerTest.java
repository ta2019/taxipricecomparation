package org.bitbucket.taxipricecomparison.test_controllers;

import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;
import org.bitbucket.taxipricecomparison.dto.AddressDto;
import org.bitbucket.taxipricecomparison.stub.WireMockStub;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MainControllerTest {
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;
    private WireMockStub stubServer;
    private String address1 = "Санкт-Петербург, Сытнинская улица, 7";
    private String address2 = "Санкт-Петербург, Улица Блохина, 6";
    private String addressFormView;
    private String callButton;

    @Before
    public void setup() {
        addressFormView = "addressForm";
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        callButton = "callService";

        String rll = "30.310694,59.957791~30.303822,59.950591";
        stubServer = new WireMockStub(address1, address2, rll);
    }

    @After
    public void shutdown() {
        stubServer.closeStub();
    }

    @Test
    public void smokeTest() {
        assertThat(context, CoreMatchers.notNullValue());
        assertThat(mockMvc, CoreMatchers.notNullValue());
    }

    // add user, add history, add notes to history, check their count and content

    @Test
    @WithMockUser
    public void redirectViewTest() throws Exception {
        mockMvc.perform(get("/")).andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/" + addressFormView));
    }

    @Test
    @WithMockUser
    public void getAddressFormTest() throws Exception {
        mockMvc.perform(get("/" + addressFormView)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(addressFormView))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attributeExists("address"))
                .andExpect(model().attribute("address", CoreMatchers.equalTo(new AddressDto())))
                .andExpect(view().name(addressFormView))
                .andExpect(content().string(containsString("From")))
                .andExpect(content().string(containsString("To")));

    }

    @Test
    @WithMockUser
    public void comparePriceNegativeAddressTest() throws Exception {
        addAllStubs();
        AddressDto addressDto = new AddressDto();
        checkBadParams(compareWithParams(addressDto));
        addressDto.setFrom("");
        addressDto.setTo("");
        checkBadParams(compareWithParams(addressDto));
        addressDto.setFrom(address1);
        addressDto.setTo(address1);
        checkBadParams(compareWithParams(addressDto));
    }

    @Test
    @WithMockUser
    public void comparePriceTest() throws Exception {
        addAllStubs();
        AddressDto addressDto = new AddressDto();
        addressDto.setFrom(address1);
        addressDto.setTo(address2);
        compareWithParams(addressDto)
                .andExpect(status().isOk())
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("allRides", CoreMatchers.notNullValue()))
                .andExpect(model().attribute("address", CoreMatchers.notNullValue()));
    }

    private void addAllStubs() {
        stubServer.addGeocoderApiStub();
        stubServer.addYandexTaxiApiStub("def", "def");
        stubServer.addUberTaxiApiStub("def", "def");
        stubServer.addGettTaxiApiStub("def", "def");
    }

    private ResultActions compareWithParams(AddressDto addressDto) throws Exception {
        return mockMvc.perform(post("/" + addressFormView).flashAttr("address", addressDto)).andDo(print());
    }

    private ResultActions checkBadParams(ResultActions mvcQuery) throws Exception {
        return mvcQuery.andExpect(model().hasErrors())
                .andExpect(model().attributeHasErrors("address"));
    }

    @Test
    @WithMockUser
    public void callIncorrectValuesButtonTest() throws Exception {
        mockMvc.perform(post("/" + callButton)).andDo(print())
                .andExpect(status().is4xxClientError());

    }

    @Test
    @WithMockUser
    public void callButtonTest() throws Exception {
        addAllStubs();
        AddressDto addressDto = new AddressDto();
        addressDto.setFrom(address1);
        addressDto.setTo(address2);
        compareWithParams(addressDto).andExpect(status().isOk());
        for (TaxiClassesEnum classes : TaxiClassesEnum.values())
            for (TaxiServiceEnum service : TaxiServiceEnum.values())
                mockMvc.perform(get("/" + callButton + "/" + classes + "/" + service)).andDo(print())
                        .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void callButtonEmptyParamTest() throws Exception {
        for (TaxiClassesEnum classes : TaxiClassesEnum.values())
            for (TaxiServiceEnum service : TaxiServiceEnum.values())
                mockMvc.perform(get("/" + callButton + "/" + classes + "/" + service)).andDo(print())
                        .andExpect(status().is3xxRedirection())
                        .andExpect(redirectedUrl(addressFormView));
    }
}
