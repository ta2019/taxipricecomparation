package org.bitbucket.taxipricecomparison;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import okhttp3.OkHttpClient;
import org.bitbucket.taxipricecomparison.api.TaxiApi;
import org.bitbucket.taxipricecomparison.api.TaxiApiYandex;
import org.bitbucket.taxipricecomparison.api.entities.RideCard;
import org.bitbucket.taxipricecomparison.api.entities.RideInfo;
import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.enums.TaxiServiceEnum;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;
import org.bitbucket.taxipricecomparison.errors.IncorrectAddressException;
import org.hamcrest.CoreMatchers;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.MatcherAssert.assertThat;

@TestPropertySource
public class ApiLocalYandexTest {
    private WireMockServer wireTaxiApiServer;
    private OkHttpClient client;

    private TaxiClassesEnum taxiClass = TaxiClassesEnum.business;
    private String rll = "30.310694,59.957791~30.303822,59.950591";

    private String addressFrom = "Санкт-Петербург, Сытнинская улица, 7";
    private String addressTo = "Санкт-Петербург, Улица Блохина, 6";

    //TODO: make https connection
    @Before
    public void configureWireMockServer() {
        wireTaxiApiServer = new WireMockServer(options().bindAddress("localhost").port(8000).httpsPort(8001));
        wireTaxiApiServer.start();
        WireMock.configureFor(wireTaxiApiServer.port());
        WireMock.configureFor(wireTaxiApiServer.httpsPort());
        client = new OkHttpClient();
    }

    @After
    public void shutdownWireMockServer() {
        if (wireTaxiApiServer != null) {
            wireTaxiApiServer.stop();
        }
    }

    @Test
    public void shouldGetResponseWithCoordinatesTest() {
        addTaxiApiStub("def", "def");

        TaxiApi yandexApiTestClass = new TaxiApiYandex(new RideInfo("59.957791", "30.310694", "59.950591", "30.303822", taxiClass));
        List<RideCard> rides = yandexApiTestClass.requestAllTariffs();
        assertThat(rides, CoreMatchers.notNullValue());
        assertionForBusinessClassRequest(rides);
    }

    @Test
    public void shouldGetResponseWithAddressTest() throws IncorrectAddressException {
        addTaxiApiStub("def", "def");
        addGeocoderApiStub();
        TaxiApi yandexApiTestClass = new TaxiApiYandex(new RideInfo(addressFrom, addressTo, taxiClass));
        List<RideCard> rides = yandexApiTestClass.requestAllTariffs();
        assertThat(rides, CoreMatchers.notNullValue());
        assertionForBusinessClassRequest(rides);
    }

    @Test
    public void shouldGetResponseWithAddressAllClassesTest() throws IncorrectAddressException {
        addTaxiApiStub("def", "def");
        addGeocoderApiStub();
        TaxiApi yandexApiTestClass = new TaxiApiYandex(new RideInfo(addressFrom, addressTo));
        List<RideCard> rides = yandexApiTestClass.requestAllTariffs();
        assertThat(rides, CoreMatchers.notNullValue());
        assertionForAllClassRequest(rides);
    }

    @Test
    public void shouldNotGetResponseEmptyAddressTest() {
        addTaxiApiStub("def", "def");
        TaxiApi yandexApiTestClass = new TaxiApiYandex(new RideInfo("", "", "", "", taxiClass));
        List<RideCard> rides = yandexApiTestClass.requestAllTariffs();
        assertThat(rides, CoreMatchers.notNullValue());
        assertThat(rides.size(), CoreMatchers.equalTo(0));
    }

    @Test
    public void shouldNotGetResponseIncorrectApiKeyTest() {
        addTaxiApiStub("def", UUID.randomUUID().toString());
        TaxiApi yandexApiTestClass = new TaxiApiYandex(new RideInfo("59.957791", "30.310694", "59.950591", "30.303822", taxiClass));
        List<RideCard> rides = yandexApiTestClass.requestAllTariffs();
        assertThat(rides, CoreMatchers.notNullValue());
        assertThat(rides.size(), CoreMatchers.equalTo(0));
    }

    @Test
    public void shouldNotGetResponseIncorrectClidTest() {
        addTaxiApiStub(UUID.randomUUID().toString(), "def");
        TaxiApi yandexApiTestClass = new TaxiApiYandex(new RideInfo("59.957791", "30.310694", "59.950591", "30.303822", taxiClass));
        List<RideCard> rides = yandexApiTestClass.requestAllTariffs();
        assertThat(rides, CoreMatchers.notNullValue());
        assertThat(rides.size(), CoreMatchers.equalTo(0));
    }

    public void addTaxiApiStub(String clid, String apikey) {
        try {
            if (clid.equals("def")) {
                clid = ApiUtils.getProperty("yandex.clid");
            }
            if (apikey.equals("def")) {
                apikey = ApiUtils.getProperty("yandex.apikey");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // TODO working regex
//        Pattern.compile("((econom)|(bussiness)|(vip)|(minivan)|(comfortplus))(,(?>((econom)|(bussiness)|(vip)|(minivan)|(comfortplus))|(\\1)))*");
        //       Pattern.compile("(x)((a)|(bc)|(def))?");
        JSONObject businessAnswer = new JSONObject("{\"currency\":\"RUB\",\"distance\":1529.7899700403214,\"options\":[{\"class_level\":70,\"class_name\":\"business\",\"class_text\":\"Комфорт\",\"min_price\":149.0,\"price\":273.0,\"price_text\":\"~273 руб.\",\"waiting_time\":97.224904537200928}],\"time\":324.48051244776155,\"time_text\":\"6 мин\"}");
        JSONObject fullAnswer = new JSONObject("{\"currency\":\"RUB\",\"distance\":1783.2100304365158,\"options\":[{\"class_level\":70,\"class_name\":\"business\",\"class_text\":\"Комфорт\",\"min_price\":149.0,\"price\":184.0,\"price_text\":\"~184 руб.\",\"waiting_time\":16.403949975967407},{\"class_level\":80,\"class_name\":\"comfortplus\",\"class_text\":\"Комфорт+\",\"min_price\":249.0,\"price\":273.0,\"price_text\":\"~273 руб.\",\"waiting_time\":164.84607154130936},{\"class_level\":50,\"class_name\":\"econom\",\"class_text\":\"Эконом\",\"min_price\":79.0,\"price\":94.0,\"price_text\":\"~94 руб.\",\"waiting_time\":103.39001888036728},{\"class_level\":95,\"class_name\":\"minivan\",\"class_text\":\"Минивэн\",\"min_price\":499.0,\"price\":620.0,\"price_text\":\"~620 руб.\",\"waiting_time\":134.9969819188118},{\"class_level\":90,\"class_name\":\"vip\",\"class_text\":\"Business\",\"min_price\":419.0,\"price\":424.0,\"price_text\":\"~424 руб.\",\"waiting_time\":94.83646559715271}],\"time\":358.70467405216669,\"time_text\":\"6 мин\"}\n");
        wireTaxiApiServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", equalTo(clid))
                .withQueryParam("apikey", equalTo(apikey))
                .withQueryParam("class", equalTo(String.valueOf(TaxiClassesEnum.business)))
                .withQueryParam("rll", equalTo(rll))
                .atPriority(2)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withBody(businessAnswer.toString())));
        wireTaxiApiServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", equalTo(clid))
                .withQueryParam("apikey", equalTo(apikey))
                .withQueryParam("class", equalTo(ApiUtils.taxiClassesJoin()))
                .withQueryParam("rll", equalTo(rll))
                .atPriority(2)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withBody(fullAnswer.toString())));
        //Bad requests
        // TODO: simplify
        wireTaxiApiServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", notMatching(clid))
                .atPriority(4)
                .willReturn(aResponse()
                        .withStatus(403)
                        .withHeader("Content-Type", "application/json; charset=utf-8")));
        wireTaxiApiServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("apikey", notMatching(apikey))
                .atPriority(4)
                .willReturn(aResponse()
                        .withStatus(403)
                        .withHeader("Content-Type", "application/json; charset=utf-8")));
        wireTaxiApiServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", equalTo(clid))
                .withQueryParam("apikey", equalTo(apikey))
                .withQueryParam("class", equalTo(".*"))
                .withQueryParam("rll", equalTo(rll))
                .atPriority(5)
                .willReturn(aResponse()
                        .withStatus(500)
                        .withHeader("Content-Type", "application/json; charset=utf-8")));
    }

    public void addGeocoderApiStub() {
        JSONObject geoCoderFirstAnswer = new JSONObject("{\"response\":{\"GeoObjectCollection\":{\"metaDataProperty\":{\"GeocoderResponseMetaData\":{\"request\":\"Санкт-Петербург, Сытнинская улица, 7\",\"results\":\"10\",\"found\":\"1\"}},\"featureMember\":[{\"GeoObject\":{\"metaDataProperty\":{\"GeocoderMetaData\":{\"precision\":\"exact\",\"text\":\"Россия, Санкт-Петербург, Сытнинская улица, 7\",\"kind\":\"house\",\"Address\":{\"country_code\":\"RU\",\"formatted\":\"Россия, Санкт-Петербург, Сытнинская улица, 7\",\"postal_code\":\"197101\",\"Components\":[{\"kind\":\"country\",\"name\":\"Россия\"},{\"kind\":\"province\",\"name\":\"Северо-Западный федеральный округ\"},{\"kind\":\"province\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"locality\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"street\",\"name\":\"Сытнинская улица\"},{\"kind\":\"house\",\"name\":\"7\"}]},\"AddressDetails\":{\"Country\":{\"AddressLine\":\"Россия, Санкт-Петербург, Сытнинская улица, 7\",\"CountryNameCode\":\"RU\",\"CountryName\":\"Россия\",\"AdministrativeArea\":{\"AdministrativeAreaName\":\"Санкт-Петербург\",\"Locality\":{\"LocalityName\":\"Санкт-Петербург\",\"Thoroughfare\":{\"ThoroughfareName\":\"Сытнинская улица\",\"Premise\":{\"PremiseNumber\":\"7\",\"PostalCode\":{\"PostalCodeNumber\":\"197101\"}}}}}}}}},\"name\":\"Сытнинская улица, 7\",\"description\":\"Санкт-Петербург, Россия\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"30.306589 59.955732\",\"upperCorner\":\"30.314799 59.95985\"}},\"Point\":{\"pos\":\"30.310694 59.957791\"}}}]}}}");
        JSONObject geoCoderSecondAnswer = new JSONObject("{\"response\":{\"GeoObjectCollection\":{\"metaDataProperty\":{\"GeocoderResponseMetaData\":{\"request\":\"Санкт-Петербург, Улица Блохина, 6\",\"results\":\"10\",\"found\":\"1\"}},\"featureMember\":[{\"GeoObject\":{\"metaDataProperty\":{\"GeocoderMetaData\":{\"precision\":\"number\",\"text\":\"Россия, Санкт-Петербург, улица Блохина, 6/3\",\"kind\":\"house\",\"Address\":{\"country_code\":\"RU\",\"formatted\":\"Россия, Санкт-Петербург, улица Блохина, 6/3\",\"postal_code\":\"197198\",\"Components\":[{\"kind\":\"country\",\"name\":\"Россия\"},{\"kind\":\"province\",\"name\":\"Северо-Западный федеральный округ\"},{\"kind\":\"province\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"locality\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"street\",\"name\":\"улица Блохина\"},{\"kind\":\"house\",\"name\":\"6/3\"}]},\"AddressDetails\":{\"Country\":{\"AddressLine\":\"Россия, Санкт-Петербург, улица Блохина, 6/3\",\"CountryNameCode\":\"RU\",\"CountryName\":\"Россия\",\"AdministrativeArea\":{\"AdministrativeAreaName\":\"Санкт-Петербург\",\"Locality\":{\"LocalityName\":\"Санкт-Петербург\",\"Thoroughfare\":{\"ThoroughfareName\":\"улица Блохина\",\"Premise\":{\"PremiseNumber\":\"6/3\",\"PostalCode\":{\"PostalCodeNumber\":\"197198\"}}}}}}}}},\"name\":\"улица Блохина, 6/3\",\"description\":\"Санкт-Петербург, Россия\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"30.299716 59.948532\",\"upperCorner\":\"30.307927 59.952651\"}},\"Point\":{\"pos\":\"30.303822 59.950591\"}}}]}}}");
        try {
            wireTaxiApiServer.stubFor(get(urlMatching("/1.x/\\?.+"))
                    .withQueryParam("apikey", equalTo(ApiUtils.getProperty("geocoder.apikey")))
                    .withQueryParam("format", equalTo("json"))
                    .withQueryParam("geocode", equalTo(addressFrom))
                    .willReturn(okJson(geoCoderFirstAnswer.toString())));
            wireTaxiApiServer.stubFor(get(urlMatching("/1.x/\\?.+"))
                    .withQueryParam("apikey", equalTo(ApiUtils.getProperty("geocoder.apikey")))
                    .withQueryParam("format", equalTo("json"))
                    .withQueryParam("geocode", equalTo(addressTo))
                    .willReturn(okJson(geoCoderSecondAnswer.toString())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void assertionForBusinessClassRequest(List<RideCard> cards) {
        assertThat(cards.size(), CoreMatchers.equalTo(1));
        for (RideCard card : cards) {
            assertThat(card.getTaxiClass(), CoreMatchers.equalTo(TaxiClassesEnum.business));
            assertThat(card.getPrice(), CoreMatchers.equalTo(273.0));
            assertThat(card.getCost(), CoreMatchers.equalTo("~273 руб."));
            assertThat(card.getTaxiService(), CoreMatchers.equalTo(TaxiServiceEnum.yandex));
        }
    }

    private void assertionForAllClassRequest(List<RideCard> cards) {
        assertThat(cards.size(), CoreMatchers.equalTo(TaxiClassesEnum.values().length));
        for (RideCard card : cards) {
            assertThat(card.getTaxiClass(), CoreMatchers.anyOf(CoreMatchers.equalTo(TaxiClassesEnum.business),
                    CoreMatchers.equalTo(TaxiClassesEnum.econom),
                    CoreMatchers.equalTo(TaxiClassesEnum.comfortplus),
                    CoreMatchers.equalTo(TaxiClassesEnum.minivan),
                    CoreMatchers.equalTo(TaxiClassesEnum.vip)
            ));
            assertThat(card.getCost(), CoreMatchers.containsString(String.valueOf((int) card.getPrice())));
            assertThat(card.getTaxiService(), CoreMatchers.equalTo(TaxiServiceEnum.yandex));
        }
    }
}
