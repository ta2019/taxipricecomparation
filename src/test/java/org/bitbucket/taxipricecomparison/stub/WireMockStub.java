package org.bitbucket.taxipricecomparison.stub;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.bitbucket.taxipricecomparison.api.enums.TaxiClassesEnum;
import org.bitbucket.taxipricecomparison.api.services.ApiUtils;
import org.json.JSONObject;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WireMockStub {
    private WireMockServer stubServer;
    private String rll;
    private String address1, address2;
    private String longAdr1 = "г. Москва",
            longAdr2 = "край Приморский, г Владивосток";

    public WireMockStub(String address1, String address2, String rll) {
        stubServer = new WireMockServer(options().bindAddress("localhost").port(8000).httpsPort(8001));
        stubServer.start();
        WireMock.configureFor(stubServer.port());
        WireMock.configureFor(stubServer.httpsPort());
        this.rll = rll;
        this.address1 = address1;
        this.address2 = address2;
    }

    public void closeStub() {
        if (stubServer != null) {
            stubServer.stop();
        }
    }

    public void addYandexTaxiApiStub(String clid, String apikey) {
        try {
            if (clid.equals("def")) {
                clid = ApiUtils.getProperty("yandex.clid");
            }
            if (apikey.equals("def")) {
                apikey = ApiUtils.getProperty("yandex.apikey");
            }
        } catch (IOException ignore) {
        }

        JSONObject businessAnswer = new JSONObject("{\"currency\":\"RUB\",\"distance\":1529.7899700403214,\"options\":[{\"class_level\":70,\"class_name\":\"business\",\"class_text\":\"Комфорт\",\"min_price\":149.0,\"price\":273.0,\"price_text\":\"~273 руб.\",\"waiting_time\":97.224904537200928}],\"time\":324.48051244776155,\"time_text\":\"6 мин\"}");
        JSONObject fullAnswer = new JSONObject("{\"currency\":\"RUB\",\"distance\":1783.2100304365158,\"options\":[{\"class_level\":70,\"class_name\":\"business\",\"class_text\":\"Комфорт\",\"min_price\":149.0,\"price\":184.0,\"price_text\":\"~184 руб.\",\"waiting_time\":16.403949975967407},{\"class_level\":80,\"class_name\":\"comfortplus\",\"class_text\":\"Комфорт+\",\"min_price\":249.0,\"price\":273.0,\"price_text\":\"~273 руб.\",\"waiting_time\":164.84607154130936},{\"class_level\":50,\"class_name\":\"econom\",\"class_text\":\"Эконом\",\"min_price\":79.0,\"price\":94.0,\"price_text\":\"~94 руб.\",\"waiting_time\":103.39001888036728},{\"class_level\":95,\"class_name\":\"minivan\",\"class_text\":\"Минивэн\",\"min_price\":499.0,\"price\":620.0,\"price_text\":\"~620 руб.\",\"waiting_time\":134.9969819188118},{\"class_level\":90,\"class_name\":\"vip\",\"class_text\":\"Business\",\"min_price\":419.0,\"price\":424.0,\"price_text\":\"~424 руб.\",\"waiting_time\":94.83646559715271}],\"time\":358.70467405216669,\"time_text\":\"6 мин\"}\n");
        stubServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", equalTo(clid))
                .withQueryParam("apikey", equalTo(apikey))
                .withQueryParam("class", equalTo(String.valueOf(TaxiClassesEnum.business)))
                .withQueryParam("rll", equalTo(rll))
                .atPriority(2)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withBody(businessAnswer.toString())));
        stubServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", equalTo(clid))
                .withQueryParam("apikey", equalTo(apikey))
                .withQueryParam("class", equalTo(ApiUtils.taxiClassesJoin()))
                .withQueryParam("rll", equalTo(rll))
                .atPriority(2)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withBody(fullAnswer.toString())));
        //Bad requests
        // TODO: simplify
        stubServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", notMatching(clid))
                .atPriority(4)
                .willReturn(aResponse()
                        .withStatus(403)
                        .withHeader("Content-Type", "application/json; charset=utf-8")));
        stubServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("apikey", notMatching(apikey))
                .atPriority(4)
                .willReturn(aResponse()
                        .withStatus(403)
                        .withHeader("Content-Type", "application/json; charset=utf-8")));
        stubServer.stubFor(get(urlMatching("/taxi_info\\?.+"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("clid", equalTo(clid))
                .withQueryParam("apikey", equalTo(apikey))
                .withQueryParam("class", equalTo(".*"))
                .withQueryParam("rll", equalTo(rll))
                .atPriority(5)
                .willReturn(aResponse()
                        .withStatus(500)
                        .withHeader("Content-Type", "application/json; charset=utf-8")));
    }

    public void addUberTaxiApiStub(String clid, String apikey) {
        try {
            if (clid.equals("def")) {
                clid = ApiUtils.getProperty("uber.clid");
            }
            if (apikey.equals("def")) {
                apikey = ApiUtils.getProperty("uber.apikey");
            }
        } catch (IOException ignore) {
        }

        JSONObject fullAnswer = new JSONObject("{\"prices\" : [\n{\"duration\":204.55036002154972,\"high_estimate\":149,\"distance\":1119.2909370660782,\"product_id\":\"f7b72136-e450-461f-b1e6-3e83ccc66742\",\"low_estimate\":139,\"localized_display_name\":\"uberSELECT\",\"estimate\":\"~149\\u2006руб.\",\"display_name\":\"uberSELECT\",\"currency_code\":\"RUB\"}\n, {\"duration\":204.55036002154972,\"high_estimate\":241,\"distance\":1119.2909370660782,\"product_id\":\"49c13c21-d476-41bf-98b2-d087005b03fe\",\"low_estimate\":239,\"localized_display_name\":\"uberXL\",\"estimate\":\"~241\\u2006руб.\",\"display_name\":\"uberXL\",\"currency_code\":\"RUB\"}\n, {\"duration\":204.55036002154972,\"high_estimate\":70,\"distance\":1119.2909370660782,\"product_id\":\"ccbdd4f6-7fe8-4dfa-9781-5e626205311a\",\"low_estimate\":69,\"localized_display_name\":\"uberX\",\"estimate\":\"~70\\u2006руб.\",\"display_name\":\"uberX\",\"currency_code\":\"RUB\"}\n, {\"duration\":204.55036002154972,\"high_estimate\":489,\"distance\":1119.2909370660782,\"product_id\":\"a5437404-27a0-47f3-a9f1-0bddbe2d967b\",\"low_estimate\":489,\"localized_display_name\":\"uberSUV\",\"estimate\":\"~489\\u2006руб.\",\"display_name\":\"uberSUV\",\"currency_code\":\"RUB\"}\n, {\"duration\":204.55036002154972,\"high_estimate\":409,\"distance\":1119.2909370660782,\"product_id\":\"38c58134-5cff-4e13-880b-0815e3ec7be5\",\"low_estimate\":409,\"localized_display_name\":\"uberBlack\",\"estimate\":\"~409\\u2006руб.\",\"display_name\":\"uberBlack\",\"currency_code\":\"RUB\"}\n]}");
        stubServer.stubFor(get(urlMatching("/v1/availability/price\\?.+"))
/*                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("", equalTo(clid))
                .withQueryParam("", equalTo(apikey))
                .withQueryParam("", equalTo(ApiUtils.taxiClassesJoin()))
                .withQueryParam("", equalTo(rll))*/
                .atPriority(2)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withBody(fullAnswer.toString())));
    }

    public void addGettTaxiApiStub(String clid, String apikey) {
        try {
            if (clid.equals("def")) {
                clid = ApiUtils.getProperty("gett.clid");
            }
            if (apikey.equals("def")) {
                apikey = ApiUtils.getProperty("gett.apikey");
            }
        } catch (IOException ignore) {
        }

        JSONObject fullAnswer = new JSONObject("{\"prices\" : [\n{\"high_estimate\":259,\"product_id\":\"f39444f0-d0ee-4e9d-a657-5d0742014d44\",\"low_estimate\":249,\"estimate\":\"~259\\u2006руб.\",\"currency\":\"RUB\",\"display_name\":\"business\"}\n, {\"high_estimate\":351,\"product_id\":\"eb6f49bc-3db5-457c-b4ca-96ca3421cab4\",\"low_estimate\":349,\"estimate\":\"~351\\u2006руб.\",\"currency\":\"RUB\",\"display_name\":\"comfortplus\"}\n, {\"high_estimate\":180,\"product_id\":\"45e936d3-45a3-4a16-ba9b-426d77ecbb58\",\"low_estimate\":179,\"estimate\":\"~180\\u2006руб.\",\"currency\":\"RUB\",\"display_name\":\"econom\"}\n, {\"high_estimate\":599,\"product_id\":\"8ebd8079-8efa-4509-aef8-eac39f1e8de0\",\"low_estimate\":599,\"estimate\":\"~599\\u2006руб.\",\"currency\":\"RUB\",\"display_name\":\"minivan\"}\n, {\"high_estimate\":519,\"product_id\":\"a4f67890-79a6-4b7e-af51-9e652f74f02a\",\"low_estimate\":519,\"estimate\":\"~519\\u2006руб.\",\"currency\":\"RUB\",\"display_name\":\"vip\"}\n]}");
        stubServer.stubFor(get(urlMatching("/v1.1/availability/price\\?.+"))
                /*              .withHeader("Accept", equalTo("application/json"))
                              .withQueryParam("clid", equalTo(clid))
                              .withQueryParam("apikey", equalTo(apikey))
                              .withQueryParam("class", equalTo(ApiUtils.taxiClassesJoin()))
                              .withQueryParam("rll", equalTo(rll))*/
                .atPriority(2)
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withBody(fullAnswer.toString())));
    }

    public void addGeocoderApiStub() {
        JSONObject geoCoderFirstAnswer = new JSONObject("{\"response\":{\"GeoObjectCollection\":{\"metaDataProperty\":{\"GeocoderResponseMetaData\":{\"request\":\"Санкт-Петербург, Сытнинская улица, 7\",\"results\":\"10\",\"found\":\"1\"}},\"featureMember\":[{\"GeoObject\":{\"metaDataProperty\":{\"GeocoderMetaData\":{\"precision\":\"exact\",\"text\":\"Россия, Санкт-Петербург, Сытнинская улица, 7\",\"kind\":\"house\",\"Address\":{\"country_code\":\"RU\",\"formatted\":\"Россия, Санкт-Петербург, Сытнинская улица, 7\",\"postal_code\":\"197101\",\"Components\":[{\"kind\":\"country\",\"name\":\"Россия\"},{\"kind\":\"province\",\"name\":\"Северо-Западный федеральный округ\"},{\"kind\":\"province\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"locality\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"street\",\"name\":\"Сытнинская улица\"},{\"kind\":\"house\",\"name\":\"7\"}]},\"AddressDetails\":{\"Country\":{\"AddressLine\":\"Россия, Санкт-Петербург, Сытнинская улица, 7\",\"CountryNameCode\":\"RU\",\"CountryName\":\"Россия\",\"AdministrativeArea\":{\"AdministrativeAreaName\":\"Санкт-Петербург\",\"Locality\":{\"LocalityName\":\"Санкт-Петербург\",\"Thoroughfare\":{\"ThoroughfareName\":\"Сытнинская улица\",\"Premise\":{\"PremiseNumber\":\"7\",\"PostalCode\":{\"PostalCodeNumber\":\"197101\"}}}}}}}}},\"name\":\"Сытнинская улица, 7\",\"description\":\"Санкт-Петербург, Россия\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"30.306589 59.955732\",\"upperCorner\":\"30.314799 59.95985\"}},\"Point\":{\"pos\":\"30.310694 59.957791\"}}}]}}}");
        JSONObject geoCoderSecondAnswer = new JSONObject("{\"response\":{\"GeoObjectCollection\":{\"metaDataProperty\":{\"GeocoderResponseMetaData\":{\"request\":\"Санкт-Петербург, Улица Блохина, 6\",\"results\":\"10\",\"found\":\"1\"}},\"featureMember\":[{\"GeoObject\":{\"metaDataProperty\":{\"GeocoderMetaData\":{\"precision\":\"number\",\"text\":\"Россия, Санкт-Петербург, улица Блохина, 6/3\",\"kind\":\"house\",\"Address\":{\"country_code\":\"RU\",\"formatted\":\"Россия, Санкт-Петербург, улица Блохина, 6/3\",\"postal_code\":\"197198\",\"Components\":[{\"kind\":\"country\",\"name\":\"Россия\"},{\"kind\":\"province\",\"name\":\"Северо-Западный федеральный округ\"},{\"kind\":\"province\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"locality\",\"name\":\"Санкт-Петербург\"},{\"kind\":\"street\",\"name\":\"улица Блохина\"},{\"kind\":\"house\",\"name\":\"6/3\"}]},\"AddressDetails\":{\"Country\":{\"AddressLine\":\"Россия, Санкт-Петербург, улица Блохина, 6/3\",\"CountryNameCode\":\"RU\",\"CountryName\":\"Россия\",\"AdministrativeArea\":{\"AdministrativeAreaName\":\"Санкт-Петербург\",\"Locality\":{\"LocalityName\":\"Санкт-Петербург\",\"Thoroughfare\":{\"ThoroughfareName\":\"улица Блохина\",\"Premise\":{\"PremiseNumber\":\"6/3\",\"PostalCode\":{\"PostalCodeNumber\":\"197198\"}}}}}}}}},\"name\":\"улица Блохина, 6/3\",\"description\":\"Санкт-Петербург, Россия\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"30.299716 59.948532\",\"upperCorner\":\"30.307927 59.952651\"}},\"Point\":{\"pos\":\"30.303822 59.950591\"}}}]}}}");
        JSONObject geoCoderLongAns1 = new JSONObject("{\n    \"response\": {\n        \"GeoObjectCollection\": {\n            \"metaDataProperty\": {\n                \"GeocoderResponseMetaData\": {\n                    \"boundedBy\": {\n                        \"Envelope\": {\n                            \"lowerCorner\": \"37.038186 55.312148\",\n                            \"upperCorner\": \"38.2026 56.190802\"\n                        }\n                    },\n                    \"request\": \"г. Москва\",\n                    \"results\": \"10\",\n                    \"found\": \"1\"\n                }\n            },\n            \"featureMember\": [\n                {\n                    \"GeoObject\": {\n                        \"metaDataProperty\": {\n                            \"GeocoderMetaData\": {\n                                \"precision\": \"other\",\n                                \"text\": \"Россия, Москва\",\n                                \"kind\": \"province\",\n                                \"Address\": {\n                                    \"country_code\": \"RU\",\n                                    \"formatted\": \"Россия, Москва\",\n                                    \"Components\": [\n                                        {\n                                            \"kind\": \"country\",\n                                            \"name\": \"Россия\"\n                                        },\n                                        {\n                                            \"kind\": \"province\",\n                                            \"name\": \"Центральный федеральный округ\"\n                                        },\n                                        {\n                                            \"kind\": \"province\",\n                                            \"name\": \"Москва\"\n                                        }\n                                    ]\n                                },\n                                \"AddressDetails\": {\n                                    \"Country\": {\n                                        \"AddressLine\": \"Россия, Москва\",\n                                        \"CountryNameCode\": \"RU\",\n                                        \"CountryName\": \"Россия\",\n                                        \"AdministrativeArea\": {\n                                            \"AdministrativeAreaName\": \"Москва\"\n                                        }\n                                    }\n                                }\n                            }\n                        },\n                        \"name\": \"Москва\",\n                        \"description\": \"Россия\",\n                        \"boundedBy\": {\n                            \"Envelope\": {\n                                \"lowerCorner\": \"36.803259 55.142221\",\n                                \"upperCorner\": \"37.96779 56.021281\"\n                            }\n                        },\n                        \"Point\": {\n                            \"pos\": \"37.622504 55.753215\"\n                        }\n                    }\n                }\n            ]\n        }\n    }\n}");
        JSONObject geoCoderLongAns2 = new JSONObject("{\n    \"response\": {\n        \"GeoObjectCollection\": {\n            \"metaDataProperty\": {\n                \"GeocoderResponseMetaData\": {\n                    \"request\": \"край Приморский, г Владивосток,\",\n                    \"results\": \"10\",\n                    \"found\": \"1\"\n                }\n            },\n            \"featureMember\": [\n                {\n                    \"GeoObject\": {\n                        \"metaDataProperty\": {\n                            \"GeocoderMetaData\": {\n                                \"precision\": \"other\",\n                                \"text\": \"Россия, Приморский край, Владивосток\",\n                                \"kind\": \"locality\",\n                                \"Address\": {\n                                    \"country_code\": \"RU\",\n                                    \"formatted\": \"Россия, Приморский край, Владивосток\",\n                                    \"Components\": [\n                                        {\n                                            \"kind\": \"country\",\n                                            \"name\": \"Россия\"\n                                        },\n                                        {\n                                            \"kind\": \"province\",\n                                            \"name\": \"Дальневосточный федеральный округ\"\n                                        },\n                                        {\n                                            \"kind\": \"province\",\n                                            \"name\": \"Приморский край\"\n                                        },\n                                        {\n                                            \"kind\": \"area\",\n                                            \"name\": \"Владивостокский городской округ\"\n                                        },\n                                        {\n                                            \"kind\": \"locality\",\n                                            \"name\": \"Владивосток\"\n                                        }\n                                    ]\n                                },\n                                \"AddressDetails\": {\n                                    \"Country\": {\n                                        \"AddressLine\": \"Россия, Приморский край, Владивосток\",\n                                        \"CountryNameCode\": \"RU\",\n                                        \"CountryName\": \"Россия\",\n                                        \"AdministrativeArea\": {\n                                            \"AdministrativeAreaName\": \"Приморский край\",\n                                            \"SubAdministrativeArea\": {\n                                                \"SubAdministrativeAreaName\": \"Владивостокский городской округ\",\n                                                \"Locality\": {\n                                                    \"LocalityName\": \"Владивосток\"\n                                                }\n                                            }\n                                        }\n                                    }\n                                }\n                            }\n                        },\n                        \"name\": \"Владивосток\",\n                        \"description\": \"Приморский край, Россия\",\n                        \"boundedBy\": {\n                            \"Envelope\": {\n                                \"lowerCorner\": \"131.84038 43.056686\",\n                                \"upperCorner\": \"132.238891 43.290031\"\n                            }\n                        },\n                        \"Point\": {\n                            \"pos\": \"131.885485 43.115536\"\n                        }\n                    }\n                }\n            ]\n        }\n    }\n}");
        stubServer.stubFor(get(urlMatching("/1.x/\\?.+"))
                .withQueryParam("geocode", equalTo(address1))
                .withQueryParam("format", equalTo("json"))
                .willReturn(okJson(geoCoderFirstAnswer.toString())));
        stubServer.stubFor(get(urlMatching("/1.x/\\?.+"))
                .withQueryParam("geocode", equalTo(address2))
                .withQueryParam("format", equalTo("json"))
                .willReturn(okJson(geoCoderSecondAnswer.toString())));
        stubServer.stubFor(get(urlMatching("/1.x/\\?.+"))
                .withQueryParam("geocode", equalTo(longAdr1))
                .withQueryParam("format", equalTo("json"))
                .willReturn(okJson(geoCoderLongAns1.toString())));
        stubServer.stubFor(get(urlMatching("/1.x/\\?.+"))
                .withQueryParam("geocode", equalTo(longAdr2))
                .withQueryParam("format", equalTo("json"))
                .willReturn(okJson(geoCoderLongAns2.toString())));
    }
}
