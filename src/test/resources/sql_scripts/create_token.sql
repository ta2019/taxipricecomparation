TRUNCATE TABLE user_roles;
TRUNCATE TABLE user_records;
DROP TABLE records;
DROP TABLE role;
DROP TABLE users;
create table users
(
    id       bigint  not null
        constraint users_pkey
            primary key,
    email    varchar(255),
    enabled  boolean not null,
    password varchar(255)
);
create table role
(
    id   bigint not null
        constraint role_pkey
            primary key,
    name varchar(255)
);
create table records
(
    id       bigint not null
        constraint records_pkey
            primary key,
    address1 varchar(255),
    address2 varchar(255),
    date     timestamp
);

INSERT INTO users (id, email, enabled, password)
values (10, 'test@gmail.com', true, '$2a$10$G2YL36KekM7T9y/Jlo1i2OnSCNARdvRGgT5YRnrPa1mLR5YQoVM6G');

INSERT INTO role
select 1, 'ROLE_USER'
where not exists(SELECT 1 from role where id=1);

INSERT INTO user_roles(user_id, role_id)
values (10, 1);

INSERT INTO verification_token(id, expiry_date, token, user_id)
values (20, '2019-10-30 00:40:02.506000', '03ac3b23-e0ec-4be1-b9a1-ce20b835166a', 10);
