TRUNCATE TABLE user_roles;
TRUNCATE TABLE user_records;
DROP TABLE records;
DROP TABLE role;
DROP TABLE users;
create table users
(
    id       bigint  not null
        constraint users_pkey
            primary key,
    email    varchar(255),
    enabled  boolean not null,
    password varchar(255)
);
create table role
(
    id   bigint not null
        constraint role_pkey
            primary key,
    name varchar(255)
);
create table records
(
    id       bigint not null
        constraint records_pkey
            primary key,
    address1 varchar(255),
    address2 varchar(255),
    date     timestamp
);


INSERT INTO users (id, email, enabled, password)
values (20, 'test2@gmail.com', true, '$2a$10$G2YL36KekM7T9y/Jlo1i2OnSCNARdvRGgT5YRnrPa1mLR5YQoVM6G');

INSERT INTO role
select 1, 'ROLE_USER'
where not exists(SELECT 1 from role where id = 1);

INSERT INTO user_roles(user_id, role_id)
values (20, 1);

INSERT INTO records(id, address1, address2, date)
values (50, 'Проспект имени Михаила Степановича', 'улица Грёз', current_timestamp);
INSERT INTO records(id, address1, address2, date)
values (51, 'Проспект фамилии Ведеркина', 'улица Танцев и Слёз', current_timestamp);
INSERT INTO records(id, address1, address2, date)
values (52, 'Бар уличного музыканта', 'улица Забалтийская', current_timestamp);

insert into user_records(user_id, record_id)
values (20, 50);
insert into user_records(user_id, record_id)
values (20, 51);
insert into user_records(user_id, record_id)
values (20, 52);
