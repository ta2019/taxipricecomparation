TRUNCATE TABLE user_roles;
TRUNCATE TABLE user_records;
DROP TABLE records;
DROP TABLE role;
DROP TABLE users;
create table users
(
    id       bigint  not null
        constraint users_pkey
            primary key,
    email    varchar(255),
    enabled  boolean not null,
    password varchar(255)
);
create table role
(
    id   bigint not null
        constraint role_pkey
            primary key,
    name varchar(255)
);
create table records
(
    id       bigint not null
        constraint records_pkey
            primary key,
    address1 varchar(255),
    address2 varchar(255),
    date     timestamp
);

/*TRUNCATE */
INSERT INTO users (id, email, enabled, password)
values (1, 'test@gmail.com', true, '$2a$10$G2YL36KekM7T9y/Jlo1i2OnSCNARdvRGgT5YRnrPa1mLR5YQoVM6G');
INSERT INTO role
values (1, 'ROLE_USER');
INSERT INTO user_roles
values (1, 1);