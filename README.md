# Сравнение цен такси
## Краткое описание проекта

Данный проект представляет собой веб-сервис (сайт). Для начала работы пользователю требуется зарегистрироваться/ залогиниться, после чего он может пользоваться сервисом. Пользователь вводит желаемые адреса отправки и прибытия, а сервис, в свою очередь, выводит цены на такси в различных компаниях в порядке возрастания. Пользователь, сделав выбор, перенаправляется на сайт этой компании (или заказывает такси, путём бронирования звонка).
---
## CI Status
[![Codeship Status for ta2019/taxipricecomparation](https://app.codeship.com/projects/a1090f80-d61c-0137-15cf-723a327e64c6/status?branch=master)](https://app.codeship.com/projects/370473)